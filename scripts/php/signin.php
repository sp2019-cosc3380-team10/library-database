<?php
    $login_error = "";
    $_COOKIE["open_signin"] = "false";

    if(isset($_POST["signin"]))
    {
        if (empty($_POST["signin-username"]) || empty($_POST{"signin-password"}))
        {
            $login_error = "Please enter a username and password.";
        }
        else
        {
            // defines user name and password
            $user = $_POST["signin-username"];
            $pass = hash("sha512", $_POST["signin-password"]);
    
            // defines db information
            include("db_credentials.php");
    
            // attempts to connects to db
            $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL!" . mysqli_connect_error());
    
            // query for selecting logins
            $query = "SELECT `LibraryNo`, `UserActive` FROM `USERS`, `LOGINS` WHERE Password='$pass' AND (UserName='$user' OR LibraryNo='$user')";

            // query to select user info
            if ($results = mysqli_query($db_connection, $query))
            {
                // finds out the number of rows from query
                $num_rows = mysqli_num_rows($results);
        
                // if there is a row that exists with these credentials, redirect to landing page
                if ($num_rows == 1)
                {
                    $row = $results->fetch_row();

                    // if user is deactivated
                    if ($row[1] == 0)
                    {
                        $login_error = "Your account has been deactivated. Please contact the administrators at the library.";
                        $_COOKIE["open_signin"] = "true";
                    }
                    else
                    {
                        // assigns a new session variable
                        $_SESSION["signin_session"] = $row[0];
                        header("Location: /landing/index.php");
                    }

                }
                else
                {
                    $login_error = "Invalid username or password.";
                    $_COOKIE["open_signin"] = "true";
                }
            }
            else
            {
                $login_error = "Unable to query database!";
                $_COOKIE["open_signin"] = "true";
            }

            // closes the connection to db
            mysqli_close($db_connection);
        }

    }
?>