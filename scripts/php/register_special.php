<?php
    $register_messages = "";
    $open_register = "";

    if (isset($_POST["register"]))
    {
        if(true)
        {
            // grabs all values from the form
            $usertype = $_POST["register-usertype"];
            $user = $_POST["register-username"];
            $email = $_POST["register-email"];
            $first = $_POST["register-firstname"];
            $last = $_POST["register-lastname"];
            $birth = $_POST["register-birthdate"];
            $address = $_POST["register-address"];
            $city = $_POST["register-city"];
            $state = $_POST["register-state"];
            $zip = $_POST["register-zip"];

            // combines the first letter of first and last name and zip code
            $pass = hash("sha512", $first[0] . $last[0] . $zip . $user);

            // hashes username and creates a library number
            $library_no = substr(hash("sha512", $user), 0, 8);

            // attempts to connects to db
            $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL!" . mysqli_connect_error());

            // query to check if username is unique
            $unique_user_query = "SELECT * FROM `USERS` WHERE UserName='$user';";

            // queries for duplicate usernames
            $duplicate_users = mysqli_query($db_connection, $unique_user_query) OR die ("Unable to query for UserName! " . mysqli_error($db_connection));
            
            // query to check if email is unique
            $unique_email_query = "SELECT * FROM `CONTACTS` WHERE Email='$email';";

            // queries for duplicate emails
            $duplicate_emails = mysqli_query($db_connection, $unique_email_query) OR die ("Unable to query for Email! " . mysqli_error($db_connectioN));
            
            // counts the number of rows
            $num_duplicate_users = mysqli_num_rows($duplicate_users);
            $num_duplicate_emails = mysqli_num_rows($duplicate_emails);

            // checks if there are no duplicate usernames
            if ($num_duplicate_users == 0 && $num_duplicate_emails == 0)
            {
                // qyery to check for duplicate library numbers
                $unique_query = "SELECT * FROM `USERS` WHERE LibraryNo='$library_no'";

                // check for duplciates and rehash library numbers
                do 
                {
                    // queries for duplicate library numbers
                    $duplicates = mysqli_query($db_connection, $unique_query) OR die ("Unable to query for LibraryNo! " . mysqli_error($db_connection));

                    // counts the number of rows
                    $num_rows = mysqli_num_rows($duplicates);

                    // if there are duplicates, rehash the library number
                    if ($num_rows > 0)
                    {
                        // uses library number to hash another number
                        $library_no = substr(hash("sha512", $library_no), 0, 8);
                    }

                } while($num_rows > 0);

                // query to insert into login
                $login_query = "INSERT INTO `LOGINS` (Password) VALUES ('$pass');";

                // query to insert into contacts
                $contact_query = "INSERT INTO `CONTACTS` (Email, StreetAddress, State, City, ZipCode) VALUES ('$email', '$address', '$state', '$city', '$zip');";

                // query to insert into users
                $user_query = "INSERT INTO `USERS` (LibraryNo, CONTACTS_ContactID, LOGINS_IDLogin, USER_TYPES_UserTypeID, UserName, FirstName, LastName, DateOfBirth) VALUES 
                    ('$library_no', 
                    (SELECT IDContact FROM `CONTACTS` WHERE Email='$email' AND StreetAddress='$address' AND State='$state' AND City='$city' AND ZipCode='$zip'), 
                    (SELECT IDLogin FROM `LOGINS` WHERE Password='$pass'), 
                    (SELECT IDUserType FROM `USER_TYPES` WHERE UserType='$usertype'), 
                    '$user', '$first', '$last', '$birth');";

                // attempts to query login info
                if (mysqli_query($db_connection, $login_query) OR die ("Unable to query user information! " . mysqli_error($db_connection)))
                {
                    // attempts to query contact info
                    if (mysqli_query($db_connection, $contact_query) OR die ("Unable to query contact info! " . mysqli_error($db_connection)))
                    {
                        // attempts to query user info
                        if (mysqli_query($db_connection, $user_query) OR die ("Unable to query user info! " . mysqli_error($db_connection)))
                        {
                            $register_messages = $register_messages . '<span class="text-success">' . $last . ", " . $first . " [" . $library_no . ", " . $user . ", " . $usertype .'] registered successfully! Please tell the user that their password is the first letter of their first name, the first letter of their last name, their zip code, and username (case sensitive).</span>';
                        }
                    }
                }
            }
            else
            {
                // print messages if there are issues
                if ($num_duplicate_emails > 0)
                {
                    $register_messages = $register_messages . '<span class="text-danger">This email is already in use. </span>';

                }
                if ($num_duplicate_users > 0)
                {
                    $register_messages = $register_messages . '<span class="text-danger">This username is already in use. </span>';
                }
                    
            }

            $open_register = "show";

            // closes connection
            mysqli_close($db_connection);
        }
    }

?>