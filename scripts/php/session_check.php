<?php
    /**
     * Checks to see if user is currently signed in or not.
     */
    session_start();

    if (!isset($_SESSION["signin_session"]))
    {
        header("Location: /");
    }
?>