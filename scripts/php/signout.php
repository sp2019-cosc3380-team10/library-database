<?php
    session_start();

    // destroys ALL sessions
    if (session_destroy())
    {
        header("location: /");
    }
?>