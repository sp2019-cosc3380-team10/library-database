<?php
    /**
     * Initializes any user data by querying the library no. and storing it into session variables.
     */

    // includes db_credentials.php to define credentials
    include("db_credentials.php");

    // attempts to connect to database
    if ($db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME))
    {
        if (isset($_SESSION["signin_session"]))
        {
            $library_no = $_SESSION["signin_session"];
    
            // query to select library no, username, firstname, and lastname
            $query = "SELECT `LibraryNo`, `UserName`, `FirstName`, `LastName`, `UserType` FROM `USERS`, `USER_TYPES` WHERE LibraryNo='$library_no' AND USER_TYPES_UserTypeID=IDUserType;";
        
            // attempts to query the database
            if ($results = mysqli_query($db_connection, $query))
            {
                // fetches query row
                $row = $results->fetch_row();
        
                // sets session to have basic user info
                $_SESSION["LibraryNo"] = $row[0];
                $_SESSION["UserName"] = $row[1];
                $_SESSION["FirstName"] = $row[2];
                $_SESSION["LastName"] = $row[3];
                $_SESSION["UserType"] = $row[4];
            }
            else
            {
                die ("Unable to query MySQL database!");
            }
        }

    }
    else
    {
        die ("Unable to connect to MySQL database!") . mysqli_connect_error();
    }
?>