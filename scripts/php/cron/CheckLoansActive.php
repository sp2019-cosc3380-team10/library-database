<?php
    /**
     * Checks to see if the current datetime is past the expire date for requests. If so, deactivate the request.
     * Checks to see if the request has a cancel date. If so, deactivate the request.
     * Checks to see if the current request has a pickup date. If so, deactivate the request.
     */

    // defomes db information
    DEFINE ("DB_HOST", "localhost");
    DEFINE ("DB_USER", "librarydb-ADMIN");
    DEFINE ("DB_PASS", "Team10Project!");
    DEFINE ("DB_NAME", "librarydb");

    // uses db credentials and establishes connection
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    // deactivates loans that are active with a check in date
    $auto_deactivate_loan_query = "UPDATE `LOANS` SET `LoanActive`=0 WHERE `CheckInDate` IS NOT NULL AND `LoanActive`=1";

    // queries auto deactivate loan
    mysqli_query($db_connection, $auto_deactivate_loan_query) or die ("Unable to auto deactivate checked in loans!");

    // queries for active loans that are currently late
    $late_loans_query = "SELECT * FROM `LOANS` WHERE `DueDate` <= NOW() AND `LoanActive`=1;" or die ("Unable to query for late loans!");

    // queries for late, active loans
    $late_loans_results = mysqli_query($db_connection, $late_loans_query);
    
    // if there is a late loan
    if ($late_loans_results->num_rows > 0)
    {
        // insert late loans into fines if there isn't a fine id associated with the loan id
        while ($late_loans_row = $late_loans_results->fetch_assoc())
        {
            // grabs all row data for querying
            $IDLoan = $late_loans_row["IDLoan"];
            $LoanNo = $late_loans_row["LoanNo"];
            $USERS_IDUser = $late_loans_row["USERS_IDUser"];
            $MEDIA_IDMedia = $late_loans_row["MEDIA_IDMedia"];

            // queries for the ID of late fine type
            $fine_type_id_results = mysqli_query($db_connection, "SELECT `IDFineType` FROM `FINE_TYPE` WHERE `FineTypeName`='late'");
            $fine_type_id_row = $fine_type_id_results->fetch_assoc();
            $IDLateType = $fine_type_id_row["IDFineType"];

            // queries for the ID of lost fine type            
            $lost_type_id_results = mysqli_query($db_connection, "SELECT `IDFineType` FROM `FINE_TYPE` WHERE `FineTypeName`='lost'");
            $lost_type_id_row = $lost_type_id_results->fetch_assoc();
            $IDLostType = $lost_type_id_row["IDFineType"];
            
            // queries for the late and lost fine of the media related to the fine
            $late_fine_query = "SELECT `IDFineCost`, `FineCost` FROM `FINE_COSTS` WHERE `MEDIA_IDMedia`='$MEDIA_IDMedia' AND `FINE_TYPE_IDFineType`='$IDLateType';";
            $lost_fine_query = "SELECT `IDFineCost`, `FineCost` FROM `FINE_COSTS` WHERE `MEDIA_IDMedia`='$MEDIA_IDMedia' AND `FINE_TYPE_IDFineType`='$IDLostType';";
            $late_fine_results = mysqli_query($db_connection, $late_fine_query);
            $lost_fine_results = mysqli_query($db_connection, $lost_fine_query);
            $late_fine_row = $late_fine_results->fetch_assoc();
            $lost_fine_row = $lost_fine_results->fetch_assoc();
            $IDLateFine = $late_fine_row["IDFineCost"];
            $IDLostFine = $lost_fine_row["IDFineCost"];
            
            // query to check for existing fine
            $existing_fine_query = "SELECT * FROM `FINES` WHERE `LOANS_IDLoan`='$IDLoan' AND `FINE_COSTS_IDFineCost`='$IDLateFine' AND `FineActive`=1;";

            // queries for existing late fines related to this loan
            $existing_fine_results = mysqli_query($db_connection, $existing_fine_query);

            // if there is a late fine related to this loan
            if ($existing_fine_results->num_rows > 0)
            {
                // if existing fine is more than a week old, change to lost fine

            }
            // else insert a new late fine
            else
            {
                // hashes a new fine number
                $FineNo = substr(hash("sha512", $LoanNo), 0 , 8);

                // query to insert new fine
                $insert_late_fine_query = "INSERT INTO `FINES` (`FineNo`, `USERS_IDUser`, `LOANS_IDLoan`, `FINE_COSTS_IDFineCost`, `FineDate`) VALUES
                    ('$FineNo', '$USERS_IDUser', '$IDLoan', '$IDLateFine', NOW());";

                // queries the database to insert a new fine
                $insert_late_fine_results = mysqli_query($db_connection, $insert_late_fine_query) or die ("Unable to create a new late fine!");
            }
        }
    }

    // closes db connection
    mysqli_close($db_connection);
?>