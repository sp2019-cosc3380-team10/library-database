<?php
    /**
     * Checks to see if the current datetime is past the expire date for requests. If so, deactivate the request.
     * Checks to see if the request has a cancel date. If so, deactivate the request.
     * Checks to see if the current request has a pickup date. If so, deactivate the request.
     */

    // defomes db information
    DEFINE ("DB_HOST", "localhost");
    DEFINE ("DB_USER", "librarydb-ADMIN");
    DEFINE ("DB_PASS", "Team10Project!");
    DEFINE ("DB_NAME", "librarydb");

    // uses db credentials and establishes connection
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    // deactivates request if user has not picked up the request
    $request_expire_query = "UPDATE `REQUESTS` SET `RequestActive`=0 WHERE NOW()>`ExpireDate` AND `RequestActive`=1 AND `PickUpDate` IS NULL;";

    // deactivates any requests that has a pickup date or cancel date and is still active
    $request_auto_deactivate = "UPDATE `REQUESTS` SET `RequestActive`=0 WHERE `PickUpDate` IS NOT NULL OR `CancelDate` IS NOT NULL AND `RequestActive`=1;";

    // queries check for expire dates
    $request_expire_results = mysqli_query($db_connection, $request_expire_query);

    // queries to deactivate requests
    $request_auto_deactivate = mysqli_query($db_connection, $request_auto_deactivate);
    
    // closes db connection
    mysqli_close($db_connection);
?>