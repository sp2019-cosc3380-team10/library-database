<?php
    if(isset($_POST["checkin"]))
    {
        if (empty($_POST["checkin-loannumber"]))
        {
            $itemnumber_error = "Please enter a loan number.";
        }
        else
        {
            // defines db information
            include("db_credentials.php");
    
            // attempts to connects to db
            $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL!" . mysqli_connect_error());
	    
	    // get input values from form
	    $loan_number = $_POST["checkin-loannumber"];
	    $checkin_datetime = new DateTime();
	    $checkin_datetime = $checkin_datetime->format('Y-m-d H:i:s');
	    
            // Define two queries: for checking if an active loan with that number exists, and for deactivating the loan and recording time checked in
            $check_exists_query = "SELECT `LoanNo` FROM `LOANS` WHERE `LoanNo`='$loan_number' AND `LoanActive`=1";
	    $checkin_query = "UPDATE `LOANS`
		SET `CheckInDate` = '$checkin_datetime', `LoanActive` = 0
		WHERE `LoanNo` = '$loan_number'";

            // run the query to validate entered loan number
            if ($results = mysqli_query($db_connection, $check_exists_query))
            {
                // finds out the number of rows from query
                $num_rows = mysqli_num_rows($results);
        
                // if there is a row that exists, edit the loan to show checked in, else return an error
                if ($num_rows == 1)
                {
			if ($results = mysqli_query($db_connection, $checkin_query))
			{
				$checkin_success_message = "Returned loan " . $loan_number;
			}
			else
			{
				$itemnumber_error = "Unable to query database with checkin! ";
			}
                }
                else
                {
			$itemnumber_error = "Invalid loan number.";
                }
            }
            else
            {
                $itemnumber_error = "Unable to query database with select!";
            }

            // closes the connection to db
            mysqli_close($db_connection);
        }

    }
?>