<?php
    $username_err = "";
    $email_err = "";
    $password_err = "";
    $confirm_err = "";
    $register_error = "";
    $_COOKIE["open_register"] = "false";

    if (isset($_POST["register"]))
    {
        if ($_POST["register-password"] != $_POST["register-confirm"])
        {
            $confirm_err = "Passwords do not match.";
        }
        else
        {
            // grabs all values from the form
            $user = $_POST["register-username"];
            $email = $_POST["register-email"];
            $pass = hash("sha512", $_POST["register-password"]);
            $first = $_POST["register-firstname"];
            $last = $_POST["register-lastname"];
            $birth = $_POST["register-birthdate"];
            $address = $_POST["register-address"];
            $city = $_POST["register-city"];
            $state = $_POST["register-state"];
            $zip = $_POST["register-zip"];

            // hashes username and creates a library number
            $library_no = substr(hash("sha512", $user), 0, 8);
    
            // defines db credentials
            include("db_credentials.php");
    
            // attempts to connects to db
            $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL!" . mysqli_connect_error());

            // query to check if username is unique and lirary no is unique
            $unique_query = "SELECT * FROM `USERS` WHERE UserName='$user';";

            // queries for duplicate usernames
            $duplicates = mysqli_query($db_connection, $unique_query) OR die ("Unable to query for UserName! " . mysqli_error($db_connection));
            
            // counts the number of rows
            $num_rows = mysqli_num_rows($duplicates);

            // checks if there are no duplicate usernames
            if ($num_rows == 0)
            {
                // qyery to check for duplicate library numbers
                $unique_query = "SELECT * FROM `USERS` WHERE LibraryNo='$library_no'";

                // check for duplciates and rehash library numbers
                do 
                {
                    // queries for duplicate library numbers
                    $duplicates = mysqli_query($db_connection, $unique_query) OR die ("Unable to query for LibraryNo! " . mysqli_error($db_connection));

                    // counts the number of rows
                    $num_rows = mysqli_num_rows($duplicates);

                    // if there are duplicates, rehash the library number
                    if ($num_rows > 0)
                    {
                        // uses library number to hash another number
                        $library_no = substr(hash("sha512", $library_no), 0, 8);
                    }

                } while($num_rows > 0);

                // query to insert into login
                $login_query = "INSERT INTO `LOGINS` (Password) VALUES ('$pass');";

                // query to insert into contacts
                $contact_query = "INSERT INTO `CONTACTS` (Email, StreetAddress, State, City, ZipCode) VALUES ('$email', '$address', '$state', '$city', '$zip');";

                // query to insert into users
                $user_query = "INSERT INTO `USERS` (LibraryNo, CONTACTS_ContactID, LOGINS_IDLogin, USER_TYPES_UserTypeID, UserName, FirstName, LastName, DateOfBirth) VALUES ('$library_no', (SELECT IDContact FROM `CONTACTS` WHERE Email='$email' AND StreetAddress='$address' AND State='$state' AND City='$city' AND ZipCode='$zip'), (SELECT IDLogin FROM `LOGINS` WHERE Password='$pass'), (SELECT IDUserType FROM `USER_TYPES` WHERE UserType='student'), '$user', '$first', '$last', '$birth');";

                // attempts to query login info
                if (mysqli_query($db_connection, $login_query) OR die ("Unable to query user information! " . mysqli_error($db_connection)))
                {
                    // attempts to query contact info
                    if (mysqli_query($db_connection, $contact_query) OR die ("Unable to query contact info! " . mysqli_error($db_connection)))
                    {
                        // attempts to query user info
                        if (mysqli_query($db_connection, $user_query) OR die ("Unable to query user info! " . mysqli_error($db_connection)))
                        {
                            // if all queries successful, set session to library number
                            $_SESSION["signin_session"] = $library_no;
                            // redirect user to landing page
                            header("Location: /landing/index.php");
                        }
                    }
                }
            }
            else
            {
                $username_err = "This username is already in use.";
            }

            // closes connection
            mysqli_close($db_connection);
        }
    }

?>