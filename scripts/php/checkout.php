<?php

    if(isset($_POST["checkout"]))
    {
        if (empty($_POST["checkout-medianumber"]) || empty($_POST["checkout-usernumber"]))
        {
            $itemnumber_error = "Please enter both a media number and a user number.";
        }
        else
        {
            // defines db information
            include("db_credentials.php");
    
            // attempts to connects to db
            $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL!" . mysqli_connect_error());
    
            // get input values from form
	    $user_number = $_POST["checkout-usernumber"];
	    $media_number = $_POST["checkout-medianumber"];
	    
	    // compute checkout and due dates
	    $checkout_datetime = new DateTime();
	    $due_datetime = clone $checkout_datetime;
	    $due_datetime->add(new DateInterval('P14D')); //Is this the actual loan time limit?
	    $checkout_datetime = $checkout_datetime->format('Y-m-d H:i:s');
	    $due_datetime = $due_datetime->format('Y-m-d H:i:s');
	    
	    // create new loan number
	    $loan_number = substr(hash("sha512", $media_number . $user_number . $checkout_datetime), 0, 8);
	    
	    // define two queries: for checking if a media with that number exists, and for checking if a user with that number exists
	    $check_user_exists_query = "SELECT `IDUser`, `UserName` FROM `USERS` WHERE `LibraryNo`='$user_number' AND `UserActive`=1";
            $check_media_exists_query = "SELECT `IDMedia`, `Title` FROM `MEDIA` WHERE `BarcodeNo`='$media_number'";
	    
            // run the query to validate entered user and media number
            if ($select_user_results = mysqli_query($db_connection, $check_user_exists_query)
		AND $select_media_results = mysqli_query($db_connection, $check_media_exists_query))
            {
                // finds out the number of rows from query
                $num_rows_media = mysqli_num_rows($select_media_results);
		$num_rows_user = mysqli_num_rows($select_user_results);
        
                // if there is a row that exists for both media and user, query to add a loan
                if ($num_rows_media == 1 && $num_rows_user == 1)
                {
			//store the found user and media
			$user_row = mysqli_fetch_assoc($select_user_results);
			$media_row = mysqli_fetch_assoc($select_media_results);
			
			$user_id = $user_row["IDUser"];
			$media_id = $media_row["IDMedia"];
			
			//compose the query for adding a new loan
			$checkout_loan_query = "INSERT INTO `LOANS`
			SET `LoanNo`='$loan_number', 
			    `USERS_IDUser`=$user_id, 
			    `MEDIA_IDMedia`=$media_id,
			    `CheckOutDate`='$checkout_datetime', 
			    `DueDate`='$due_datetime', 
			    `LoanActive`=1";
			if ($checkout_results = mysqli_query($db_connection, $checkout_loan_query))
			{
				$checkout_success_message = "Checked out media \"" . $media_row["Title"] . "\" to user \"" . $user_row["UserName"] . "\"";
			}
			else
			{
				$itemnumber_error = "Unable to query database with checkout!" . $checkout_loan_query;
			}
                }
                elseif ($num_rows_media == 1)
                {
			$itemnumber_error = "Invalid user number.";
                }
		elseif ($num_rows_user == 1)
                {
			$itemnumber_error = "Invalid media number.";
                }
		else
                {
			$itemnumber_error = "Invalid media number and user number.";
                }
            }
            else
            {
                $itemnumber_error = "Unable to query database with select!";
            }

            // closes the connection to db
            mysqli_close($db_connection);
        }

    }
?>