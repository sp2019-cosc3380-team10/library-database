<?php
    /**
     * Checks to see if user is an admin. If not, redirect back to landing page.
     */

    // if user type is not admin, redirect to landing page
    if ($_SESSION["UserType"] != "admin")
    {
        header("Location: /landing/");
    }
?>