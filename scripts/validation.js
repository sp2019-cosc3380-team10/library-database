/**
 * @file Used to uphold validity constraints.
 * @author Alvn Huynh
 */



/**
 * Checks if element has less characters than the limit.
 * @param {*} element - HTML element that is being referred to.
 * @param {int} limit - The inclusive maximum limit of characters.
 * @author Alvin Huynh
 */
function CheckCharLimit(element, limit)
{
    if(element.value.length > limit) {
        element.value = element.value.substr(0, limit);
    }
}

/**
 * Checks if the value of the input element is alphanumeric (only has digits and letters), dashes '-', and underscores '_'. Removes any invalid characters.
 * @param {HTMLInputElement} element 
 * @author Alvin Huynh
 */
function CheckAlphaNumeric(element)
{
    element.value = element.value.replace(/[^a-zA-Z0-9_-]/gi, "");
}

/**
 * Checks if the value of the input element is alphanumeric (only has digits and letters), dashes '-', spaces ' ', and underscores '_'. Removes any invalid characters.
 * @param {HTMLInputElement} element 
 * @author Alvin Huynh
 */
function CheckEmail(element)
{
    element.value = element.value.replace(/[^A-Za-z0-9-_+.@]/gi, "");
}

/**
 * Checks if the value of the input element is alphabetic (only has letters). Removes any invalid characters.
 * @param {HTMLInputElement} element 
 * @author ALvin Huynh
 */
function CheckAlpha(element)
{
    element.value = element.value.replace(/[^a-zA-Z]/gi, "");
}

/**
 * Checks if the value of the input element is numeric (only has digits). Removes any invalid characters.
 * @param {HTMLInputElement} element 
 * @author Alvin Huynh
 */
function CheckNumeric(element)
{
    element.value = element.value.replace(/[^0-9]/gi, "");
}

/**
 * 
 * @param {HTMLInputElement} element 
 * @author Alvin Huynh
 */
function CheckAddress(element)
{
    element.value = element.value.replace(/[^A-Za-z0-9 -',.]/gi, "");
}

/**
 * Checks if the value of the input value matches the regex string. Automatically replaces any invalid characters.
 * @param {RegExp} regex 
 * @param {HTMLInputElement} element 
 * @author Alvin Huynh
 */
function CheckRegex(regex, element)
{
    element.value = element.value.replace(regex, element);
}

$(document).ready(function() {
    $(function(){
        $("#register-username").on("keydown keyup", function(event) {
            CheckAlphaNumeric(this);
        });

        $("#register-email").on("keydown keyup", function(event) {
            CheckEmail(this);
        });

        $("#register-address").on("keydown keyup", function(event){
            CheckAddress(this);
        });
        
        $("#register-zip").on("keydown keyup", function(event) {
            CheckNumeric(this);
        });
    });
});