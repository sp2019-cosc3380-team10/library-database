/**
 * @file Used for redirecting webpages when a user clicks on certain buttons.
 * @author Alvin Huynh
 */

$(document).ready(function() {
    var redirect = document.createElement("a");
    redirect.setAttribute("target", "_self");

    $("#loans-btn").on("click", function(){
        redirect.setAttribute("href", "loans.php");
        redirect.click();
    });

    $("#requests-btn").on("click", function(){
        redirect.setAttribute("href", "requests.php");
        redirect.click();
        
    });

    $("#fines-btn").on("click", function(){
        redirect.setAttribute("href", "fines.php");
        redirect.click();
        
    });

    $("#account-btn").on("click", function(){
        redirect.setAttribute("href", "account.php");
        redirect.click();
        
    });

    $("#search-btn").on("click", function(){
        redirect.setAttribute("href", "search.php");
        redirect.click();
    });

    $("#checkout-return-btn").on("click", function(){
        redirect.setAttribute("href", "checkout_return.php");
        redirect.click();
    });

    $("#media-manager-btn").on("click", function(){
        redirect.setAttribute("href", "media_manager.php");
        redirect.click();
    });

    $("#requests-manager-btn").on("click", function(){
        redirect.setAttribute("href", "requests_manager.php");
        redirect.click();
    });

    $("#fines-manager-btn").on("click", function(){
        redirect.setAttribute("href", "fines_manager.php");
        redirect.click();
    });

    $("#user-manager-btn").on("click", function(){
        redirect.setAttribute("href", "user_manager.php");
        redirect.click();
    });

    $("#statistics-btn").on("click", function(){
        redirect.setAttribute("href", "statistics.php");
        redirect.click();
    });
});
