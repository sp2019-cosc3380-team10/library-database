<?php
    // TODO: MEDIA MANAGER
    //       Querying code right now from database. Working on adding Media
    //       Manager functionality.

    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

    $db_connection = @mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL " . mysqli_connect_error());

    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/admin_check.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Media Manager</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>
    <body>
        <?php
            // includes navigation bar and session
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");

            // sets default display cards to none
            $results_style_modify = "display: none;";
            $results_style_remove = "display: none;";
            $info_modify = "display: none;";
            $item_to_mod = "display: none;";


            if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"])) {
                // remove the style for results card
                $results_style_modify = "";
            }
            if (isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
                // remove the style for results card
                $results_style_remove = "";
            }

            if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"]) || isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
                // stores GET paramters locally
                if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"])) {
                    $SearchBy = $_GET["SearchByModify"];
                    $SearchKey = $_GET["SearchKeyModify"];
                }
                if (isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
                    $SearchBy = $_GET["SearchByRemove"];
                    $SearchKey = $_GET["SearchKeyRemove"];
                }

                if ($SearchBy == "Genre") {
                    $search_query = "SELECT `BarcodeNo`, `Title`, `PublishYear`, `Quantity` FROM `MEDIA`, `MEDIA_has_GENRES`, `GENRES` WHERE
                    ;";
                }
                else if ($SearchBy == "Creator") {
                    // empty...
                }
                else if ($SearchBy == "Publisher") {
                    // empty..
                }
                else {
                    // search query to find keys like $SearchKey
                    $search_query = "SELECT `BarcodeNo`, `Title`, `PublishYear`, `QuantityAvailable`, `QuantityTotal` FROM `MEDIA` WHERE `$SearchBy` LIKE '%$SearchKey%';";
                    // queries database
                    $search_results = mysqli_query($db_connection, $search_query) or die ("Unable to query database for search results! " . mysqli_error($db_connection));
                }
            }

            $BarcodeNo = "";
            // if user selected a media
            if (isset($_GET["SelectMedia"])) {
                // changes style so the modify bar can handle requests
                $info_modify = "";

                // sets BarcodeNo
                $BarcodeNo = $_GET["SelectMedia"];

                /********* SELECT GENERAL MEDIA COLUMNS **********/
                // query for media info
                $media_query = "SELECT * FROM `MEDIA` WHERE `BarcodeNo`='$BarcodeNo';";
                // queries for media
                $media_results = mysqli_query($db_connection, $media_query);
                // grabs a row
                $media_row = $media_results->fetch_assoc();
                // grabs IDMedia
                $IDMedia = $media_row["IDMedia"];

                /*********** SELECT MEDIA TYPE ******************/
                // selects media type name
                $media_type_query = "SELECT `MediaTypeName` FROM `MEDIA_TYPES`, `MEDIA` WHERE `MEDIA_TYPES_IDMediaType`=`IDMediaType`;";
                // queries for media type
                $media_type_results = mysqli_query($db_connection, $media_type_query);
                // grabs media type row
                $media_type_row = $media_type_results->fetch_assoc();

                /************* SELECT CREATORS ****************/
                // query to select creators
                $creators_query = "SELECT `FirstName`, `LastName`, `CreatorType` FROM `CREATORS`, `MEDIA_has_CREATORS` WHERE `MEDIA_IDMedia`='$IDMedia' AND `CREATORS_IDCreator`=`IDCreator`;";
                // queries for creators
                $creators_results = mysqli_query($db_connection, $creators_query) or die("Unable to query creators! " . mysqli_error($db_connection));
                // grabs creator for current media
                $creators_row = $creators_results->fetch_assoc();

                /**************** SELECT GENRES ******************/
                // query to select genres
                $genres_query = "SELECT `GenreName` FROM `GENRES`, `MEDIA_has_GENRES` WHERE `MEDIA_IDMedia`='$IDMedia' AND `GENRES_IDGenre`=`IDGenre`;";
                // queries for genres
                $genres_results = mysqli_query($db_connection, $genres_query) or die("Unable to query genres! " . mysqli_error($db_connection));
                // grabs genre for current media
                $genres_row = $genres_results->fetch_assoc();

                /****************** SELECT PUBLISHERS ************/
                // query to select publisher names
                $publishers_query = "SELECT `PublisherName` FROM `PUBLISHERS`, `MEDIA_has_PUBLISHERS` WHERE `MEDIA_IDMedia`='$IDMedia' AND `PUBLISHERS_IDPublisher`=`IDPublisher`;";
                // queries for publishers
                $publishers_results = mysqli_query($db_connection, $publishers_query) or die("Unable to query publishers! " . mysqli_error($db_connection));
                // grabs publisher for current media
                $publisher_row = $publishers_results->fetch_assoc();

                /**************** SELECT FINE COSTS ************/
                // query to select cost for current media - damaged = actual price
                $fines_query = "SELECT `FineCost` FROM `FINE_COSTS` WHERE `MEDIA_IDMedia`='$IDMedia' AND `FINE_TYPE_IDFineType`='3';";
                // queries for cost
                $fines_results = mysqli_query($db_connection, $fines_query) or die("Unable to query fines! " . mysqli_error($db_connection));
                // grabs cost for current media
                $fines_row = $fines_results->fetch_assoc();

                // if admin finalized modification
                if (isset($_GET["MediaModified"])) {
                    $item_to_mod = "";
                }
            }
        ?>


        <!-- *********************** ADD NEW MEDIA LIBRARY ************************** -->

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Add New Media to Library</h6>
                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="form-group">
                            <label>Media Type:</label><br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="MediaTID" class="custom-control-input" value="1">
                                <label class="custom-control-label" for="customRadioInline1">Book</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="MediaTID" class="custom-control-input" value="2">
                                <label class="custom-control-label" for="customRadioInline2">Film</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline3" name="MediaTID" class="custom-control-input" value="3">
                                <label class="custom-control-label" for="customRadioInline3">CD</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Title of New Media:</label>
                            <input type="text" class="form-control" placeholder="Title" name="MediaTitle" required>
                        </div>
                        <div class="form-group">
                            <label>Publish Year:</label>
                            <input type="text" class="form-control" placeholder="Year" name="MediaPublishYear" required>
                        </div>
                        <div class="form-row">
                            <label>Creator(s):</label><br>
                            <div class="form-group col-md-5">
                                <input type="text" class="form-control" name="CreatorFirst" placeholder="First Name" required>
                            </div>
                            <div class="form-group col-md-5">
                                <input type="text" class="form-control" name="CreatorLast" placeholder="Last Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Creator Type:</label><br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline4" name="CreatorType" class="custom-control-input" value="Author">
                                <label class="custom-control-label" for="customRadioInline4">Author</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline5" name="CreatorType" class="custom-control-input" value="Director">
                                <label class="custom-control-label" for="customRadioInline5">Director</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Genres:</label><br>
                            <select class="form-control" id="genre" name="Genre" required>
                                <option selected hidden disabled>Select a genre...</option>
                                <option value="Action">Action</option>
                                <option value="Adult">Adult</option>
                                <option value="Adventure">Adventure</option>
                                <option value="Children">Children</option>
                                <option value="Comedy">Comedy</option>
                                <option value="Detective">Detective</option>
                                <option value="Drama">Drama</option>
                                <option value="Dystopia">Dystopia</option>
                                <option value="Fantasy">Fantasy</option>
                                <option value="Haiku">Haiku</option>
                                <option value="Horror">Horror</option>
                                <option value="Musical">Musical</option>
                                <option value="Mystery">Mystery</option>
                                <option value="Play">Play</option>
                                <option value="Romance">Romance</option>
                                <option value="Satire">Satire</option>
                                <option value="Science Fiction">Science Fiction</option>
                                <option value="Textbook">Textbook</option>
                                <option value="Thriller">Thriller</option>
                                <option value="Western">Western</option>
                                <option value="Young Adult">Young Adult</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Publisher:</label>
                            <input type="text" class="form-control" placeholder="Publisher" name="Publisher" required>
                        </div>
                        <div class="form-group">
                            <label>Quantity to be Received:</label>
                            <input type="number" class="form-control" placeholder="Total Quantity" name="TotalQuantity" min="1" max="20" required>
                        </div>
                        <label>Price of Media:</label><br>
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="number" class="form-control" name="Price" min="1" max="500" aria-label="Amount (to the nearest dollar)" required>
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                        <br>
                        <button type="Submit" name="AddMedia" class="btn btn-outline-secondary">Add</button>
                        <?php
                            /************* ADDING MEDIA ************/
                            if (isset($_POST["AddMedia"])) {
                                // get all values for media input by admin
                                $media_tid = $_POST['MediaTID'];
                                $media_title = $_POST['MediaTitle'];
                                $barcode_num = substr(hash("sha512", $media_title), 0, 8);
                                $pub_year = $_POST['MediaPublishYear'];
                                $creator_firstname = $_POST['CreatorFirst'];
                                $creator_lastname = $_POST['CreatorLast'];
                                $creator_type = $_POST['CreatorType'];
                                $genre = $_POST['Genre'];
                                $publisher = $_POST['Publisher'];
                                // lost, damaged, late
                                $media_cost = $_POST['Price'];
                                $media_damaged = (float) ($media_cost / 2);
                                $media_late = (float) ($media_cost / 5);
                                // total quantity will be quantity available since 
                                $total_quant = $_POST['TotalQuantity'];
                                $quant_avail = $_POST['TotalQuantity'];


                                // add new media not existing in library
                                $add = "INSERT INTO MEDIA (MEDIA_TYPES_IDMediaType, BarcodeNo, Title, PublishYear, QuantityAvailable, QuantityTotal) VALUES ((SELECT IDMediaType FROM MEDIA_TYPES WHERE IDMediaType=" . $media_tid . "),'" . $barcode_num . "','" . $media_title . "'," . $pub_year . "," . $quant_avail . "," . $total_quant . ")";

                                if (mysqli_query($db_connection, $add)) {
                                    echo "<p class=\"text-success\">" . $media_title . " was added succesfully!</p>";
                                } // else check for success taken out - sql query passed tests


                                // add new CREATOR not existent in library
                                // see if creator is being duplicated
                                $unique_creator_query = "SELECT * FROM CREATORS WHERE FirstName='$creator_firstname' AND LastName='$creator_lastname';";

                                // queries for duplicate creators
                                $duplicate_creators = mysqli_query($db_connection, $unique_creator_query) OR die ("Unable to query for Creator! " . mysqli_error($db_connection));

                                // counts the number of rows
                                $num_duplicate_creators = mysqli_num_rows($duplicate_creators);

                                // checks if there are no duplicate creators
                                if ($num_duplicate_creators == 0) {
                                    $add_creator = "INSERT INTO CREATORS (FirstName, LastName, CreatorType) VALUES ('" . $creator_firstname . "','" . $creator_lastname . "','" . $creator_type . "')";
                                    if (mysqli_query($db_connection, $add_creator)) {}
                                }

                                // add new PUBLISHER not existent in library
                                // see if publisher is being duplicated
                                $unique_publisher_query = "SELECT * FROM PUBLISHERS WHERE PublisherName='$publisher';";

                                // queries for duplicate publishers
                                $duplicate_publishers = mysqli_query($db_connection, $unique_publisher_query) OR die ("Unable to query for Publisher! " . mysqli_error($db_connection));

                                // counts the number of rows
                                $num_duplicate_publishers = mysqli_num_rows($duplicate_publishers);

                                // checks if there are no duplicate publishers
                                if ($num_duplicate_publishers == 0) {
                                    $add_publisher = "INSERT INTO PUBLISHERS (PublisherName) VALUES ('" . $publisher . "')";
                                    if (mysqli_query($db_connection, $add_publisher)) {}
                                }


                                // make connections in MEDIA_has_CREATORS/PUBLISHERS/GENRES
                                $has_creators = "INSERT INTO MEDIA_has_CREATORS VALUES ((SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $barcode_num . "'), (SELECT IDCreator FROM CREATORS WHERE LastName='" . $creator_lastname . "'));";
                                if (mysqli_query($db_connection, $has_creators)) {}

                                $has_genres = "INSERT INTO MEDIA_has_GENRES (MEDIA_IDMedia, GENRES_IDGenre) VALUES ((SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $barcode_num . "'), (SELECT IDGenre FROM GENRES WHERE GenreName='" . $genre . "'));";
                                if (mysqli_query($db_connection, $has_genres)) { echo "added has genre";}

                                $has_publishers = "INSERT INTO MEDIA_has_PUBLISHERS (MEDIA_IDMedia, PUBLISHERS_IDPublisher) VALUES ((SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $barcode_num . "'), (SELECT IDPublisher FROM PUBLISHERS WHERE PublisherName='" . $publisher . "'));";
                                if (mysqli_query($db_connection, $has_publishers)) {}

                                // adding fines for media being added
                                $price_late = "INSERT INTO FINE_COSTS (FineCost, MEDIA_IDMedia, FINE_TYPE_IDFineType) VALUES (" . $media_late . ",(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $barcode_num . "'),(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='late'));";
                                $price_damaged = "INSERT INTO FINE_COSTS (FineCost, MEDIA_IDMedia, FINE_TYPE_IDFineType) VALUES (" . $media_damaged . ",(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $barcode_num . "'),(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='damage'));";
                                $price_lost = "INSERT INTO FINE_COSTS (FineCost, MEDIA_IDMedia, FINE_TYPE_IDFineType) VALUES (" . $media_cost . ",(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $barcode_num . "'),(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='lost'));";
                                if (mysqli_query($db_connection, $price_late)) {}
                                if (mysqli_query($db_connection, $price_damaged)) {}
                                if (mysqli_query($db_connection, $price_lost)) {}
                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

        <!-- *********************** MODIFY MEDIA ************************** -->

        <!-- Searching for Media -->
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Modify Media in Library</h6>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-3">
                                <select name="SearchByModify" id="search-by" class="custom-select" required>
                                    <option value="">Search catalog by...</option>
                                    <option value="Title">Title</option>
                                    <option value="BarcodeNo">Barcode Number</option>
                                    <option value="PublishYear">Publish Year</option>
                                    <option value="Creator">Creator</option>
                                    <option value="Genre">Genre</option>
                                    <option value="Publisher">Publisher</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input name="SearchKeyModify" id="search-key" class="form-control" placeholder="Enter search key for media that will be modified..." required>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Search results to be shown -->
        <div class="container" style="<?php echo $results_style_modify; ?>">
            <div class="card">
                <div class="card-header">
                    <h5>Choose Media to Modify</h5>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <?php
                            // if SearchByModify and SearchKeyModify is set
                            if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"])) {
                                // if there are more than one row
                                if ($search_results->num_rows > 0) {
                                    // prints the table header
                                    echo '
                                        <table class="table table-hover table-striped">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Select to Modify</th>
                                                    <th scope="col">Barcode No.</th>
                                                    <th scope="col">Title</th>
                                                    <th scope="col">Publish Year</th>
                                                    <th scope="col">Qty. Available</th>
                                                    <th scope="col">Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';

                                    // fetch all the rows
                                    while($search_results_row = $search_results->fetch_assoc()) {
                                        $barcode_no = $search_results_row["BarcodeNo"];

                                        // searches for the media types
                                        $media_type_query = "SELECT MediaTypeName FROM `MEDIA_TYPES`, `MEDIA` WHERE `IDMediaType`=`MEDIA_TYPES_IDMediaType` AND `BarcodeNo`='$barcode_no';";

                                        // queries for media type
                                        $media_type_results = mysqli_query($db_connection, $media_type_query);

                                        $media_type_row = $media_type_results->fetch_assoc();

                                        echo '
                                        <tr>
                                            <td><button type="submit" name="SelectMedia" value="' . $search_results_row["BarcodeNo"] . '" class="btn btn-outline-secondary">+</button></td>
                                            <td>' . $search_results_row["BarcodeNo"] . '</td>
                                            <td>' . $search_results_row["Title"] . '</td>
                                            <td>' . $search_results_row["PublishYear"] . '</td>
                                            <td>' . $search_results_row["QuantityAvailable"] . '</td>
                                            <td>' . $media_type_row["MediaTypeName"] . '</td>
                                        </tr>
                                        ';
                                    }

                                    echo '</tbody></table>';
                                }
                                else {
                                    echo '<h4 class="text-center">No search results...</h4>';
                                }
                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

        <!-- Options to what part of the media chosen to be modified -->
        <div class="container" style="<?php echo $info_modify; ?>">
            <div class="card">
                <div class="card-header">
                    <h5>Modying <?php if(isset($_GET["SelectMedia"])) {echo "\"" . $media_row["Title"] . "\"";} ?></h5>
                </div>
                <div class="card-body">
                    <form action="" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Title:</label>
                                <input type="text" class="form-control" name="newTitle" value="<?php echo $media_row["Title"]; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Total Quantity:</label>
                                <input type="number" class="form-control" name="newTotalQuantity" min="1" max="20" value="<?php echo $media_row["QuantityTotal"]; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Creator(s):</label>
                            <input type="text" class="form-control" name="newCreator" value="<?php echo $creators_row["FirstName"] . " " . $creators_row["LastName"]; ?>">
                        </div>
                        <div class="form-group">
                            <label>Current Creator Type: <?php echo $creators_row['CreatorType']?></label><br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline6" name="newCreatorType" class="custom-control-input" value="Author">
                                <label class="custom-control-label" for="customRadioInline6">Author</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline7" name="newCreatorType" class="custom-control-input" value="Director">
                                <label class="custom-control-label" for="customRadioInline7">Director</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Publisher:</label>
                            <input type="text" class="form-control" name="newPublisher" value="<?php echo $publisher_row["PublisherName"]; ?>">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Cost:</label>
                                <input type="number" class="form-control" name="newCost" min="1" max="500" value="<?php echo $fines_row["FineCost"]; ?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Publish Year:</label>
                                <input type="text" class="form-control" name="newPublishYear" value="<?php echo $media_row["PublishYear"]; ?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Genre:</label>
                                    <select class="form-control" name="newGenre" required>
                                        <option selected hidden disabled><?php echo $genres_row["GenreName"]; ?> | Select a different genre...</option>
                                        <option value="Action">Action</option>
                                        <option value="Adult">Adult</option>
                                        <option value="Adventure">Adventure</option>
                                        <option value="Children">Children</option>
                                        <option value="Comedy">Comedy</option>
                                        <option value="Detective">Detective</option>
                                        <option value="Drama">Drama</option>
                                        <option value="Dystopia">Dystopia</option>
                                        <option value="Fantasy">Fantasy</option>
                                        <option value="Haiku">Haiku</option>
                                        <option value="Horror">Horror</option>
                                        <option value="Musical">Musical</option>
                                        <option value="Mystery">Mystery</option>
                                        <option value="Play">Play</option>
                                        <option value="Romance">Romance</option>
                                        <option value="Satire">Satire</option>
                                        <option value="Science Fiction">Science Fiction</option>
                                        <option value="Textbook">Textbook</option>
                                        <option value="Thriller">Thriller</option>
                                        <option value="Western">Western</option>
                                        <option value="Young Adult">Young Adult</option>
                                    </select>
                            </div>
                        </div>
                        <button type="Submit" name="ModifyMedia" class="btn btn-outline-secondary">Modify</button>

                        <?php 
                            if (isset($_POST["ModifyMedia"])) {
                                // get all values for media input by admin
                                $media_title = $_POST['newTitle'];
                                $pub_year = $_POST['newPublishYear'];
                                $creator_name = $_POST['newCreator'];
                                $creator_first_last = explode(" ", $creator_name);
                                $creator_type = $_POST['newCreatorType'];
                                $genre = $_POST['newGenre'];
                                $publisher = $_POST['newPublisher'];
                                // lost, damaged, late
                                $media_cost = $_POST['newCost'];
                                $media_damaged = (float) ($media_cost / 2);
                                $media_late = (float) ($media_cost / 5);
                                // total quantity - increment quantity available by new - old 
                                $total_quant = $_POST['newTotalQuantity'];
                                $avail_quant = ($total_quant - $media_row['QuantityTotal']) + $media_row['QuantityAvailable'];

                                // Modify Title WORKS
                                if ($media_row['Title'] != $media_title) {
                                    $update_title = "UPDATE MEDIA SET Title='" . $media_title . "' WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "';";

                                    if (mysqli_query($db_connection, $update_title)) {
                                        echo "<p class=\"text-success\">Title has succesfully been updated. " . $media_title . " is new title.</p>";
                                    }
                                }

                                // Modify Total Quantity WORKS
                                if ($media_row['QuantityTotal'] != $total_quant) {
                                    echo "<p>You want to update the total quantity</p>";
                                    $update_title = "UPDATE MEDIA SET QuantityTotal=" . $total_quant . ", QuantityAvailable=" . $avail_quant . " WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "';";

                                    if (mysqli_query($db_connection, $update_title)) {
                                        echo "<p class=\"text-success\">Total and available quantity have succesfully been updated for " . $media_title . ".</p>";
                                    } else {
                                        echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                    }
                                }
                         
                                // Modify Creator WORKS
                                if ($creators_row['FirstName'] != $creator_first_last[0] || $creators_row['LastName'] != $creator_first_last[1]) {
                                    echo "<p>You want to update the creator name</p>";
                                    // see if new creator is in database
                                    $unique_creator_query = "SELECT * FROM CREATORS WHERE FirstName='$creator_first_last[0]' AND LastName='$creator_first_last[1]';";
                                    // queries for duplicate creators
                                    $duplicate_creators = mysqli_query($db_connection, $unique_creator_query) OR die ("Unable to query for Creator! " . mysqli_error($db_connection));
                                    // counts the number of rows
                                    $num_duplicate_creators = mysqli_num_rows($duplicate_creators);

                                    if ($num_duplicate_creators == 0) {
                                        // check if creator type was chosen for new creator
                                        if ($creator_type != '') {
                                            // add new creator
                                            $add_creator = "INSERT INTO CREATORS (FirstName, LastName, CreatorType) VALUES ('" . $creator_first_last[0] . "','" . $creator_first_last[1] . "','" . $creator_type . "')";
                                            if (mysqli_query($db_connection, $add_creator)) {
                                                echo "<p class=\"text-success\">New Creator has been added.</p>";
                                            } else {
                                                echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                            }
                                            // establish relationship betwen media and creator
                                            $has_creators = "INSERT INTO MEDIA_has_CREATORS VALUES ((SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "'), (SELECT IDCreator FROM CREATORS WHERE FirstName='" . $creator_first_last[0] . "' AND LastName='" . $creator_first_last[1] . "'));";
                                            if (mysqli_query($db_connection, $has_creators)) {
                                                echo "<p class=\"text-success\">relationship between new creator and media has been established.</p>";
                                            } else {
                                                echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                            }
                                        } else {
                                            echo "<p class=\"bg-warning\">Creator Type must be chosen when modifying Creator.</p>";
                                        }
                                    } else {
                                        // check if relationship exists between Media and existent creator
                                        $creator_has_query = "SELECT MEDIA_IDMedia FROM MEDIA_has_CREATORS WHERE MEDIA_IDMedia=" . $media_row['IDMedia'] . ";";
                                        // queries for Media ID in relationship table
                                        $creator_has_exists = mysqli_query($db_connection, $creator_has_query) OR die ("Unable to query for MEDIA_has_CREATORS! " . mysqli_error($db_connection));
                                        // counts number of instances of Media ID in relationship table
                                        $num_creator_has = mysqli_num_rows($creator_has_exists);

                                        if ($num_creator_has == 0) {
                                           $has_creators = "INSERT INTO MEDIA_has_CREATORS VALUES ((SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "'), (SELECT IDCreator FROM CREATORS WHERE FirstName='" . $creator_first_last[0] . "' AND LastName='" . $creator_first_last[1] . "'));";
                                            if (mysqli_query($db_connection, $has_creators)) {
                                                echo "<p class=\"text-success\">New Creator Media relatioship has been inserted.</p>";
                                            } else {
                                                echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                            }
                                        } else {
                                            $update_creator_has = "UPDATE MEDIA_has_CREATORS SET CREATORS_IDCreator=(SELECT IDCreator FROM CREATORS WHERE FirstName='" . $creator_first_last[0] . "' AND LastName='" . $creator_first_last[1] . "') WHERE MEDIA_IDMedia=(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "');";

                                            if (mysqli_query($db_connection, $update_creator_has)) {
                                                echo "<p class=\"text-success\">Creator already existed, new relationship has been established.</p>";
                                            } else {
                                                echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                            }
                                        }
                                    }
                                }

                                // Modify Publisher WORKS
                                if ($publisher_row['PublisherName'] != $publisher) {
                                    echo "<p>You want to update the publisher</p>";
                                    // see if publisher is being duplicated
                                    $unique_publisher_query = "SELECT * FROM PUBLISHERS WHERE PublisherName='$publisher';";
                                    // queries for duplicate publishers
                                    $duplicate_publishers = mysqli_query($db_connection, $unique_publisher_query) OR die ("Unable to query for Publisher! " . mysqli_error($db_connection));
                                    // counts the number of rows
                                    $num_duplicate_publishers = mysqli_num_rows($duplicate_publishers);

                                    // checks if there are no duplicate publishers
                                    if ($num_duplicate_publishers == 0) {
                                        $add_publisher = "INSERT INTO PUBLISHERS (PublisherName) VALUES ('" . $publisher . "')";
                                        if (mysqli_query($db_connection, $add_publisher)) {
                                            echo "<p class=\"text-success\">New publisher has been added.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }

                                        $has_publishers = "UPDATE MEDIA_has_PUBLISHERS SET PUBLISHERS_IDPublisher=(SELECT IDPublisher FROM PUBLISHERS WHERE PublisherName='" . $publisher_row['PublisherName'] . "') WHERE MEDIA_IDMedia=(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "');";
                                        if (mysqli_query($db_connection, $has_publishers)) {
                                            echo "<p class=\"text-success\">New publisher relationship has been established.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                    } else {
                                        // check if books has relationship with a publisher
                                        $publisher_has_query = "SELECT MEDIA_IDMedia FROM MEDIA_has_PUBLISHERS WHERE MEDIA_IDMedia=" . $media_row['IDMedia'] . ";";
                                        // queries for Media ID in relationship table
                                        $publisher_has_exists = mysqli_query($db_connection, $publisher_has_query) OR die ("Unable to query for Publisher! " . mysqli_error($db_connection));
                                        // counts number of instances of Media ID in relationship table
                                        $num_pub_exists = mysqli_num_rows($publisher_has_exists);

                                        // inserting new relationship in MEDIA_has_PUBLISHERS
                                        if ($num_pub_exists == 0) {
                                            $has_publishers = "INSERT INTO MEDIA_has_PUBLISHERS (MEDIA_IDMedia, PUBLISHERS_IDPublisher) VALUES ((SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "'), (SELECT IDPublisher FROM PUBLISHERS WHERE PublisherName='" . $publisher . "'));";
                                            if (mysqli_query($db_connection, $has_publishers)) {
                                                echo "<p class=\"text-success\">Publisher already exists, new relationship has been INSERTED.</p>";
                                            } else {
                                                echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                            }
                                        } else {
                                            // a relationship exists, updating it
                                            $update_publishers_has = "UPDATE MEDIA_has_PUBLISHERS SET PUBLISHERS_IDPublisher=(SELECT IDPublisher FROM PUBLISHERS WHERE PublisherName='" . $publisher . "') WHERE MEDIA_IDMedia=(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "');";

                                            if (mysqli_query($db_connection, $update_publishers_has)) {
                                                echo "<p class=\"text-success\">Publisher already exists, new relationship has been established.</p>";
                                            } else {
                                                echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                            }
                                        }
                                    }
                                }

                                // Modify Price/Cost and Fine Costs WORKS
                                if ($fines_row['FineCost'] != $media_cost) {
                                    echo "<p>You want to update the price</p>";
                                    // check if Media has existent fine costs
                                    $curr_fine_cost = "SELECT MEDIA_IDMedia FROM FINE_COSTS WHERE MEDIA_IDMedia=" . $media_row['IDMedia'] . ";";
                                    // query for fine costs
                                    $find_fine_cost = mysqli_query($db_connection, $curr_fine_cost) OR die ("Unable to query for FineCOSTS! " . mysqli_error($db_connection));
                                    // count number of instances Media are in FINE_COSTS
                                    $num_fine_cost = mysqli_num_rows($find_fine_cost);

                                    // insert fine costs for Media
                                    if ($num_fine_cost == 0) {
                                        // adding fines for media being added
                                        $price_late = "INSERT INTO FINE_COSTS (FineCost, MEDIA_IDMedia, FINE_TYPE_IDFineType) VALUES (" . $media_late . ",(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "'),(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='late'));";
                                        $price_damaged = "INSERT INTO FINE_COSTS (FineCost, MEDIA_IDMedia, FINE_TYPE_IDFineType) VALUES (" . $media_damaged . ",(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "'),(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='damage'));";
                                        $price_lost = "INSERT INTO FINE_COSTS (FineCost, MEDIA_IDMedia, FINE_TYPE_IDFineType) VALUES (" . $media_cost . ",(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "'),(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='lost'));";
                                        if (mysqli_query($db_connection, $price_late)) {
                                            echo "<p class=\"text-success\">New Fine Cost late has been inserted.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                        if (mysqli_query($db_connection, $price_damaged)) {
                                            echo "<p class=\"text-success\">New Fine Cost damaged has been inserted.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                        if (mysqli_query($db_connection, $price_lost)) {
                                            echo "<p class=\"text-success\">New Fine Cost lost has been inserted.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                    } else {
                                        // Fine costs exist for media, updating them
                                        $update_damaged = "UPDATE FINE_COSTS SET FineCost=" . $media_damaged . " WHERE MEDIA_IDMedia=(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "') AND FINE_TYPE_IDFineType='2';";
                                        $update_lost = "UPDATE FINE_COSTS SET FineCost=" . $media_cost . " WHERE MEDIA_IDMedia=(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "') AND FINE_TYPE_IDFineType='3';";
                                        $update_late = "UPDATE FINE_COSTS SET FineCost=" . $media_late . " WHERE MEDIA_IDMedia=(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "') AND FINE_TYPE_IDFineType='1';";

                                        if (mysqli_query($db_connection, $update_damaged)) {
                                            echo "<p class=\"text-success\">Fine Cost (damaged) for media has been updated.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                        if (mysqli_query($db_connection, $update_lost)) {
                                            echo "<p class=\"text-success\">Fine Cost (lost) for media has been updated.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                        if (mysqli_query($db_connection, $update_late)) {
                                            echo "<p class=\"text-success\">Fine Cost (late) for media has been updated.</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                    }
                                }

                                // Modify Publish Year WORKS
                                if ($media_row['PublishYear'] != $pub_year) {
                                    echo "<p>You want to update the publish year</p>";
                                    $update_title = "UPDATE MEDIA SET PublishYear=" . $pub_year . " WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "';";

                                    if (mysqli_query($db_connection, $update_title)) {
                                        echo "<p class=\"text-success\">Publish year has succesfully been updated for " . $media_title . ".</p>";
                                    } else {
                                        echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                    }
                                }

                                // Modify Genre
                                if ($genres_row['GenreName'] != $genre && $genre != '') {
                                    echo "<p>You want to update the genre</p>";
                                    // check if Media has existent genre
                                    $genre_query = "SELECT MEDIA_IDMedia FROM MEDIA_has_GENRES WHERE MEDIA_IDMedia=" . $media_row['IDMedia'] . ";";
                                    // query for Media's genre
                                    $find_genre_has = mysqli_query($db_connection, $genre_query) OR die ("Unable to query for FineCOSTS! " . mysqli_error($db_connection));
                                    // count number of instances Media are in relationship table MEDIA_has_GENRES
                                    $num_genre_has = mysqli_num_rows($find_genre_has);

                                    // Media does not have genre
                                    if ($num_genre_has == 0) {
                                        $has_genres = "INSERT INTO MEDIA_has_GENRES (MEDIA_IDMedia, GENRES_IDGenre) VALUES ((SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "'), (SELECT IDGenre FROM GENRES WHERE GenreName='" . $genre . "'));";
                                        if (mysqli_query($db_connection, $has_genres)) { 
                                            echo "<p class=\"text-success\">Genre has succesfully been inserted for " . $media_title . ".</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                    } else {
                                        $update_genre = "UPDATE MEDIA_has_GENRES SET GENRES_IDGenre=(SELECT IDGenre FROM GENRES WHERE GenreName='" . $genre . "') WHERE MEDIA_IDMedia=(SELECT IDMedia FROM MEDIA WHERE BarcodeNo='" . $media_row['BarcodeNo'] . "');";

                                        if (mysqli_query($db_connection, $update_genre)) {
                                            echo "<p class=\"text-success\">Genre has succesfully been updated for " . $media_title . ".</p>";
                                        } else {
                                            echo "OOPS: " . mysqli_error($db_connection) . "<br>";
                                        }
                                    }
                                }
                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

        <!-- *********************** REMOVE MEDIA ************************** -->

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Remove Media in Library</h6>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-3">
                                <select name="SearchByRemove" id="search-by" class="custom-select" required>
                                    <option value="">Search catalog by...</option>
                                    <option value="Title">Title</option>
                                    <option value="BarcodeNo">Barcode Number</option>
                                    <option value="PublishYear">Publish Year</option>
                                    <option value="Creator">Creator</option>
                                    <option value="Genre">Genre</option>
                                    <option value="Publisher">Publisher</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input name="SearchKeyRemove" id="search-key" class="form-control" placeholder="Enter a search key..." required>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Search results to be shown in the same card body -->
        <div class="container" style="<?php echo $results_style_remove; ?>">
            <div class="card">
                <div class="card-header">
                    <h6>Choose Media to Remove</h6>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <?php
                            // if SearchByModify and SearchKeyModify is set
                            if (isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
                                // if there are more than one row
                                if ($search_results->num_rows > 0) {
                                    // prints the table header
                                    echo '
                                        <table class="table table-hover table-striped">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Select to Modify</th>
                                                    <th scope="col">Barcode No.</th>
                                                    <th scope="col">Title</th>
                                                    <th scope="col">Publish Year</th>
                                                    <th scope="col">Qty. Available</th>
                                                    <th scope="col">Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';

                                    // fetch all the rows
                                    while($search_results_row = $search_results->fetch_assoc()) {
                                        $barcode_no = $search_results_row["BarcodeNo"];

                                        // searches for the media types
                                        $media_type_query = "SELECT MediaTypeName FROM `MEDIA_TYPES`, `MEDIA` WHERE `IDMediaType`=`MEDIA_TYPES_IDMediaType` AND `BarcodeNo`='$barcode_no';";

                                        // queries for media type
                                        $media_type_results = mysqli_query($db_connection, $media_type_query);

                                        $media_type_row = $media_type_results->fetch_assoc();

                                        echo '
                                        <tr>
                                            <td><button type="submit" name="SelectMedia" value="' . $search_results_row["BarcodeNo"] . '" class="btn btn-outline-secondary">+</button></td>
                                            <td>' . $search_results_row["BarcodeNo"] . '</td>
                                            <td>' . $search_results_row["Title"] . '</td>
                                            <td>' . $search_results_row["PublishYear"] . '</td>
                                            <td>' . $search_results_row["QuantityAvailable"] . '</td>
                                            <td>' . $media_type_row["MediaTypeName"] . '</td>
                                        </tr>
                                        ';
                                    }

                                    echo '</tbody></table>';
                                }
                                else {
                                    echo '<h4 class="text-center">No search results...</h4>';
                                }
                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

    <?php
        mysqli_close($db_connection);
    ?>
    </body>
    <footer></footer>
</html>
