<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_init.php");
?>


<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Landing</title>
        <?php
            // includes default header files
	        include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php"); 
        ?>

        <script type="text/javascript" src="/scripts/landing.js"></script>
    </head>

    <body>
        <?php
            // includes navbar
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>

        <!-- Navigation button group -->
        <div class="container text-center">
            
            <div class="alert alert-primary">
                Welcome to the Team 10 Library, 
                <?php echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"]; ?>!
            </div>

            <div class="card card-body">
                <!-- First row -->
                <div class="row">
                    <div class="col-md-3">
                        <button id="loans-btn" class="btn btn-sq-lg btn-outline-secondary">
                            Loans
                            <br>
                            <i class="fas fa-book"></i>
                        </button>
                    </div>
                    <div class="col-md-3">
                        <button id="requests-btn" class="btn btn-sq-lg btn-outline-secondary">
                            Requests
                            <br>
                            <i class="fas fa-tasks"></i>
                        </button>
                    </div>
                    <div class="col-md-3">
                        <button id="fines-btn" class="btn btn-sq-lg btn-outline-secondary">
                            Fines
                            <br>
                            <i class="fas fa-dollar-sign"></i>
                        </button>
                    </div>
                    <div class="col-md-3">
                        <button id="account-btn" class="btn btn-sq-lg btn-outline-secondary">
                            Account
                            <br>
                            <i class="fas fa-user"></i>
                        </button>
                    </div>
                </div>

                <!-- Second row -->
                <div class="row">
                    <div class="col-md-3">
                        <button id="search-btn" class="btn btn-sq-lg btn-outline-secondary">
                            Search Catalog
                            <br>
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                    <?php
                        if ($_SESSION["UserType"] == "admin")
                        {
                            echo'
        
                                <div class="col-md-3">
                                    <button id="checkout-return-btn" class="btn btn-sq-lg btn-outline-secondary">
                                        Checkout/Return
                                        <br>
                                        <i class="fas fa-shopping-cart"></i>
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <button id="media-manager-btn" class="btn btn-sq-lg btn-outline-secondary">
                                        Media Manager
                                        <br>
                                        <i class="fas fa-book-open"></i>
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <button id="requests-manager-btn" class="btn btn-sq-lg btn-outline-secondary">
                                        Requests Manager
                                        <br>
                                        <i class="fas fa-address-book"></i>
                                    </button>
                                </div>
                            ';
                        }
                    ?>
                </div>
                <?php
                    if ($_SESSION["UserType"] == "admin")
                    {
                        echo '
                            <!-- Third row -->
                            <div class="row">
                                <div class="col-md-3">
                                    <button id="fines-manager-btn" class="btn btn-sq-lg btn-outline-secondary">
                                        Fines Manager
                                        <br>
                                        <i class="fas fa-hand-holding-usd"></i>
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <button id="user-manager-btn" class="btn btn-sq-lg btn-outline-secondary">
                                        User Manager
                                        <br>
                                        <i class="fas fa-users-cog"></i>
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <button id="statistics-btn" class="btn btn-sq-lg btn-outline-secondary">
                                        Library Statistics
                                        <br>
                                        <i class="fas fa-chart-pie"></i>
                                    </button>
                                </div>
                            </div>
                        ';
                    }
                ?>

            </div> 
        </div>

    </body>


    <footer>

    </footer>
</html>