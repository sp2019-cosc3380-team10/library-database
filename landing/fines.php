<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

    // establishes connection to the database
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    // stores library no
    $LibraryNo = $_SESSION["LibraryNo"];

    // query for the active fines related to the user
    $active_fines_query = "SELECT * FROM `FINES` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo') AND `FineActive`='1';";

    // query for past fines related to the user
    $past_fines_query = "SELECT * FROM `FINES` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo') AND `FineActive`='0';";

    // grabs the results of the queries
    $active_fines_results = mysqli_query($db_connection, $active_fines_query);
    $past_fines_results = mysqli_query($db_connection, $past_fines_query);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Fines</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Active Fines</h6>
                </div>
                <div class="card-body">                       
                    <?php
                        if ($active_fines_results->num_rows > 0)
                        {
                            echo '
                                <table class="table table-small table-striped table-hover">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Fine No.</th>
                                            <th scope="col">Fine Date</th>
                                            <th scope="col">Loan No.</th>
                                            <th scope="col">Media No.</th>
                                            <th scope="col">Fine Cost</th>
                                            <th scope="col">Fine Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            ';

                            while ($active_fines_row = $active_fines_results->fetch_assoc())
                            {
                                // gets ID of loan
                                $IDLoan = $active_fines_row["LOANS_IDLoan"];
                                // gets ID of fine cost
                                $IDFineCost = $active_fines_row["FINE_COSTS_IDFineCost"];

                                // queries for loan no.
                                $loan_no_query = "SELECT * FROM `LOANS` WHERE `IDLoan`='$IDLoan';";
                                $loan_no_results = mysqli_query($db_connection, $loan_no_query);
                                $loan_no_row = $loan_no_results->fetch_assoc();
                                
                                // gets ID of MEDIA
                                $IDMedia = $loan_no_row["MEDIA_IDMedia"];
                                
                                // queries for media no.
                                $media_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                $media_no_results = mysqli_query($db_connection, $media_no_query);
                                $media_no_row = $media_no_results->fetch_assoc();
                                
                                // queries for fine cost
                                $fine_cost_query = "SELECT * FROM `FINE_COSTS` WHERE `IDFineCost`='$IDFineCost';";
                                $fine_cost_results = mysqli_query($db_connection, $fine_cost_query);
                                $fine_cost_row = $fine_cost_results->fetch_assoc();

                                // gets the ID of fine type
                                $IDFineType = $fine_cost_row["FINE_TYPE_IDFineType"];

                                // queries for fine type
                                $fine_type_query = "SELECT `FineTypeName` FROM `FINE_TYPE` WHERE `IDFineType`='$IDFineType'";
                                $fine_type_results = mysqli_query($db_connection, $fine_type_query);
                                $fine_type_row = $fine_type_results->fetch_assoc();

                                echo '
                                    <tr>
                                        <td>' . $active_fines_row["FineNo"] . '</td>
                                        <td>' . $active_fines_row["FineDate"] . '</td>
                                        <td>' . $loan_no_row["LoanNo"] . '</td>
                                        <td>' . $media_no_row["BarcodeNo"] . '</td>
                                        <td>$' . $fine_cost_row["FineCost"] . '</td>
                                        <td>' . $fine_type_row["FineTypeName"] . '</td>
                                    <tr>
                                ';
                            }

                            echo '<tbody></table>';
                        }
                        else
                        {
                            echo '<h4>Wow!</h4><br>You have no fines!';
                        }
                    ?>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="accordion" id="accordion">
                <div class="card">
                    <div class="card-header">
                        <h6 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-past">
                                Past Fines
                            </button>
                        </h6>
                    </div>
                    <div id="collapse-past" class="collapse" data-parent="#accordion">
                        <div class="card-body">                       
                            <?php
                                if ($past_fines_results->num_rows > 0)
                                {
                                    echo '
                                        <table class="table table-small table-striped table-hover">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Fine No.</th>
                                                    <th scope="col">Fine Date</th>
                                                    <th scope="col">Loan No.</th>
                                                    <th scope="col">Media No.</th>
                                                    <th scope="col">Fine Cost</th>
                                                    <th scope="col">Fine Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';

                                    while ($past_fines_row = $past_fines_results->fetch_assoc())
                                    {
                                        // gets ID of loan
                                        $IDLoan = $past_fines_row["LOANS_IDLoan"];
                                        // gets ID of fine cost
                                        $IDFineCost = $past_fines_row["FINE_COSTS_IDFineCost"];
        
                                        // queries for loan no.
                                        $loan_no_query = "SELECT * FROM `LOANS` WHERE `IDLoan`='$IDLoan';";
                                        $loan_no_results = mysqli_query($db_connection, $loan_no_query);
                                        $loan_no_row = $loan_no_results->fetch_assoc();
                                        
                                        // gets ID of MEDIApast_fines_row
                                        $IDMedia = $loan_no_row["MEDIA_IDMedia"];
                                        
                                        // queries for media no.
                                        $media_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                        $media_no_results = mysqli_query($db_connection, $media_no_query);
                                        $media_no_row = $media_no_results->fetch_assoc();
                                        
                                        // queries for fine cost
                                        $fine_cost_query = "SELECT * FROM `FINE_COSTS` WHERE `IDFineCost`='$IDFineCost';";
                                        $fine_cost_results = mysqli_query($db_connection, $fine_cost_query);
                                        $fine_cost_row = $fine_cost_results->fetch_assoc();
        
                                        // gets the ID of fine type
                                        $IDFineType = $fine_cost_row["FINE_TYPE_IDFineType"];
        
                                        // queries for fine type
                                        $fine_type_query = "SELECT `FineTypeName` FROM `FINE_TYPE` WHERE `IDFineType`='$IDFineType'";
                                        $fine_type_results = mysqli_query($db_connection, $fine_type_query);
                                        $fine_type_row = $fine_type_results->fetch_assoc();
        
                                        echo '
                                            <tr>
                                                <td>' . $past_fines_row["FineNo"] . '</td>
                                                <td>' . $past_fines_row["FineDate"] . '</td>
                                                <td>' . $loan_no_row["LoanNo"] . '</td>
                                                <td>' . $media_no_row["BarcodeNo"] . '</td>
                                                <td>$' . $fine_cost_row["FineCost"] . '</td>
                                                <td>' . $fine_type_row["FineTypeName"] . '</td>
                                            <tr>
                                        ';
                                    }

                                    echo '<tbody></table>';
                                }
                                else
                                {
                                    echo '<div class="text-center"><h4>Wow!</h4>No past fines.</div>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

    <footer>

    </footer>
</html>


<?php
    mysqli_close($db_connection);
?>