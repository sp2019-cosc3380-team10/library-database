<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

    // establishes connection to the database
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    // sets default display cards to none
    $results_style = "display: none;";
    $info_style = "display: none;";

    // if there are parameters SearchBy and SearchKey
    if (isset($_GET["SearchBy"]) && isset($_GET["SearchKey"]))
    {
        // remove the style for results card
        $results_style = "";

        // stores GET paramters locally
        $SearchBy = $_GET["SearchBy"];
        $SearchKey = $_GET["SearchKey"];

        if ($SearchBy == "Genre")
        {
            $search_query = "SELECT `BarcodeNo`, `Title`, `PublishYear`, `Quantity` FROM `MEDIA`, `MEDIA_has_GENRES`, `GENRES` WHERE
            ;";
        }
        else if ($SearchBy == "Creator")
        {

        }
        else if ($SearchBy == "Publisher")
        {

        }
        else
        {
            // search query to find keys like $SearchKey
            $search_query = "SELECT `BarcodeNo`, `Title`, `PublishYear`, `QuantityAvailable`, `QuantityTotal` FROM `MEDIA` WHERE `$SearchBy` LIKE '%$SearchKey%';";

            // queries database
            $search_results = mysqli_query($db_connection, $search_query) or die ("Unable to query database for search results! " . mysqli_error($db_connection));
        }

    }

    $BarcodeNo = "";

    // if user selected a media
    if (isset($_POST["SelectMedia"]))
    {
        // changes style so the info card shows
        $info_style = "";

        // sets BarcodeNo
        $BarcodeNo = $_POST["SelectMedia"];

        /**
         * SELECT GENERAL MEDIA COLUMNS
         */

        // query for media info
        $media_query = "SELECT * FROM `MEDIA` WHERE `BarcodeNo`='$BarcodeNo';";

        // queries for media
        $media_results = mysqli_query($db_connection, $media_query);

        // grabs a row
        $media_row = $media_results->fetch_assoc();

        // grabs IDMedia
        $IDMedia = $media_row["IDMedia"];

        /**
         * SELECT MEDIA TYPE
         */
        // selects media type name
        $media_type_query = "SELECT `MediaTypeName` FROM `MEDIA_TYPES`, `MEDIA` WHERE `MEDIA_TYPES_IDMediaType`=`IDMediaType`;";

        // queries for media type
        $media_type_results = mysqli_query($db_connection, $media_type_query);
        
        // grabs media type row
        $media_type_row = $media_type_results->fetch_assoc();

        /**
         * SELECT CREATORS
         */

        // query to select creators
        $creators_query = "SELECT `FirstName`, `LastName`, `CreatorType` FROM `CREATORS`, `MEDIA_has_CREATORS` WHERE `MEDIA_IDMedia`='$IDMedia' AND `CREATORS_IDCreator`=`IDCreator`;";

        // queries for creators
        $creators_results = mysqli_query($db_connection, $creators_query) or die("Unable to query genres! " . mysqli_error($db_connection));

        /**
         * SELECT GENRES
         */
        // query to select genres
        $genres_query = "SELECT `GenreName` FROM `GENRES`, `MEDIA_has_GENRES` WHERE `MEDIA_IDMedia`='$IDMedia' AND `GENRES_IDGenre`=`IDGenre`;";

        // queries for genres
        $genres_results = mysqli_query($db_connection, $genres_query) or die("Unable to query genres! " . mysqli_error($db_connection));

        /**
         * SELECT PUBLISHERS
         */
        // query to select publisher names
        $publishers_query = "SELECT `PublisherName` FROM `PUBLISHERS`, `MEDIA_has_PUBLISHERS` WHERE `MEDIA_IDMedia`='$IDMedia' AND `PUBLISHERS_IDPublisher`=`IDPublisher`;";

        // queries for publishers
        $publishers_results = mysqli_query($db_connection, $publishers_query) or die("Unable to query genres! " . mysqli_error($db_connection));

    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Search</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Search Catalog</h6>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-3">
                                <select name="SearchBy" id="search-by" class="custom-select" required>
                                    <option value="">Search catalog by...</option>
                                    <option value="Title">Title</option>
                                    <option value="BarcodeNo">Barcode Number</option>
                                    <option value="PublishYear">Publish Year</option>
                                    <option value="Creator">Creator</option>
                                    <option value="Genre">Genre</option>
                                    <option value="Publisher">Publisher</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input name="SearchKey" id="search-key" class="form-control" placeholder="Enter a search key..." required>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container" style="<?php echo $results_style; ?>">
            <div class="card">
                <div class="card-header">
                    <h6>Search Results</h6>
                </div>
                <div class="card-body">
                    <form action="" method="POST">
                        <?php
                            // if SearchBy and SearchKey is set
                            if (isset($_GET["SearchBy"]) && isset($_GET["SearchKey"]))
                            {
                                // if there are more than one row
                                if ($search_results->num_rows > 0)
                                {
                                    // prints the table header
                                    echo '
                                        <table class="table table-striped table-hover">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Select</th>
                                                    <th scope="col">Barcode No.</th>
                                                    <th scope="col">Title</th>
                                                    <th scope="col">Publish Year</th>
                                                    <th scope="col">Qty. Available</th>
                                                    <th scope="col">Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';

                                    // fetch all the rows
                                    while($search_results_row = $search_results->fetch_assoc())
                                    {
                                        $barcode_no = $search_results_row["BarcodeNo"];

                                        // searches for the media types
                                        $media_type_query = "SELECT MediaTypeName FROM `MEDIA_TYPES`, `MEDIA` WHERE `IDMediaType`=`MEDIA_TYPES_IDMediaType` AND `BarcodeNo`='$barcode_no';";

                                        // queries for media type
                                        $media_type_results = mysqli_query($db_connection, $media_type_query);

                                        $media_type_row = $media_type_results->fetch_assoc();

                                        echo '
                                        <tr>
                                            <td><button type="submit" name="SelectMedia" value="' . $search_results_row["BarcodeNo"] . '" class="btn btn-outline-secondary">+</button></td>
                                            <td>' . $search_results_row["BarcodeNo"] . '</td>
                                            <td>' . $search_results_row["Title"] . '</td>
                                            <td>' . $search_results_row["PublishYear"] . '</td>
                                            <td>' . $search_results_row["QuantityAvailable"] . '</td>
                                            <td>' . $media_type_row["MediaTypeName"] . '</td>
                                        </tr>
                                        ';
                                    }

                                    echo '</tbody></table>';
                                }
                                else
                                {
                                    echo '<h4 class="text-center">No search results...</h4>';
                                }

                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

        <div class="container" style="<?php echo $info_style; ?>">
            <div class="card">
                <div class="card-header">
                    <h6>Selected Media Information</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h6>Barcode No.</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo $media_row["BarcodeNo"];
                                }
                            ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Title</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo $media_row["Title"];
                                }
                            ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Type</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo ucfirst($media_type_row["MediaTypeName"]);
                                }
                            ?>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <h6>Creators</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo "<ul>";
                                    while ($creators_rows = $creators_results->fetch_assoc())
                                    {
                                        echo "<li>" . 
                                            $creators_rows["FirstName"] .
                                            " " .
                                            $creators_rows["LastName"] .
                                            " (" .
                                            $creators_rows["CreatorType"] .
                                            ")" .
                                            "</li>";
                                    }
                                    echo "</ul>";
                                }
                            ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Publish Year</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo $media_row["PublishYear"];
                                }
                            ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Genres</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo "<ul>";
                                    while ($genres_row = $genres_results->fetch_assoc())
                                    {
                                        echo "<li>" . $genres_row["GenreName"] . "</li>";
                                    }
                                    echo "</ul>";
                                }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <h6>Publishers</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo "<ul>";
                                    while ($publishers_row = $publishers_results->fetch_assoc())
                                    {
                                        echo "<li>" . 
                                            $publishers_row["PublisherName"] .
                                            "</li>";
                                    }
                                    echo "</ul>";
                                }
                            ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Quantity Available</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo $media_row["QuantityAvailable"];
                                }
                            ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Quantity Total</h6>
                            <?php
                                if (isset($_POST["SelectMedia"]))
                                {
                                    echo $media_row["QuantityTotal"];
                                }
                            ?>
                        </div>
                    </div>
                    <div class="container text-center">
                        <form action="requests.php" method="POST">
                            <input style="display: none;" name="BarcodeNo" value="<?php if(isset($_POST["SelectMedia"])) echo $media_row["BarcodeNo"]; ?>">
                            <button type="submit" class="btn btn-outline-primary">Create New Request</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>

    </body>

    <footer>

    </footer>
</html>


<?php
    // closes the connection to the database
    mysqli_close($db_connection);
?>