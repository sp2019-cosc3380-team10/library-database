<?php

    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");



    // includes db credentials

    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");



    // establishes connection with the database

    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);



    $AcceptRequestMessage = "";

    if (isset($_POST["AcceptRequest"])) {
        $request_number = $_POST["AcceptRequest"];

        $date = date('Y-m-d H:i:s'); 

        $expire_date= date('Y-m-d H:i:s', strtotime($date. ' + 2 days'));

        $update_values = "UPDATE `REQUESTS` SET `ReadyDate` = NOW(), `ExpireDate`='$expire_date' WHERE `RequestNo`='$request_number'";

        mysqli_query($db_connection, $update_values) or die ("Unable to accept request!");

        $AcceptRequestMessage = '<div class="container"><div class="alert alert-success"><strong>Success:</strong> The request ' . $RequestNo . ' has been accepted.</div></div>';



    }

    $Pick_Up_Message = "";

    if (isset($_POST["Pick_Up"])) {
        $request_number = $_POST["Pick_Up"];

        $update_pick_up = "UPDATE `REQUESTS` SET `PickUpDate` = NOW(), `RequestActive`= 0 WHERE `RequestNo`='$request_number'";

        mysqli_query($db_connection, $update_pick_up) or die ("Unable to accept request!");

        $Pick_Up_Message = '<div class="container"><div class="alert alert-success"><strong>Success:</strong> The request ' . $RequestNo . ' has been accepted.</div></div>';
    }

    // cancel request message

    $CancelRequestMessage = "";

    // if cancel request

    if (isset($_POST["CancelRequest"]))

    {

        // gets request number

        $RequestNo = $_POST["CancelRequest"];



        // query update to the request

        $request_cancel_query = "UPDATE `REQUESTS` SET `CancelDate`=NOW(), `RequestActive`=0 WHERE `RequestNo`='$RequestNo';";

        mysqli_query($db_connection, $request_cancel_query);



        $CancelRequestMessage = '<div class="container"><div class="alert alert-danger"><strong>Canceled:</strong> The request ' . $RequestNo . ' has been canceled.</div></div>';

    }



    



    // gets library number from session

    $LibraryNo = $_SESSION["LibraryNo"];



    // queries for active and past requests

    $active_requests_query = "SELECT * FROM `REQUESTS` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo' AND `RequestActive`='1' AND `ReadyDate` IS NULL);";

    $past_requests_query = "SELECT * FROM `REQUESTS` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo' AND `RequestActive`='0' 
    AND `ReadyDate` IS NOT NULL AND `ExpireDate` IS NOT NULL);";

    $pending_requests_query = "SELECT * FROM `REQUESTS` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo' AND `RequestActive`='1' AND `ReadyDate` IS NOT NULL);";



    // queries statements into database

    $active_requests_results = mysqli_query($db_connection, $active_requests_query);

    $past_requests_results = mysqli_query($db_connection, $past_requests_query);

    $pending_requests_results = mysqli_query($db_connection, $pending_requests_query);



?>



<!DOCTYPE html>

<html>

    <head>

        <title>T10LIB - Request Manager</title>

        <?php

            // includes header.php

            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");

        ?>

    </head>



    <body>

        <?php

            // includes navbar.php

            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");



            echo $AcceptRequestMessage;
            echo $CancelRequestMessage;

        ?>



        <div class="container">

            <div class="card">

                <div class="card-header">

                    <h6>Active Requests</h6>

                </div>

                <div class="card-body">

                    <form action="" method="POST">                       

                        <?php

                            if ($active_requests_results->num_rows > 0)

                            {

                                echo '

                                    <table class="table table-striped table-hover">

                                        <thead class="thead-dark">

                                            <tr>

                                                <th scope ="col">ID Request</th>

                                                <th scope="col">Request No.</th>

                                                <th scope="col">Media No.</th>

                                                <th scope="col">Request Date</th>

                                                <th scope="col">Accept</th>

                                                <th scope="col">Decline</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                ';



                                while ($active_requests_row = $active_requests_results->fetch_assoc())

                                {

                                    // gets media ID

                                    $IDMedia = $active_requests_row["MEDIA_IDMedia"];



                                    // query for media barcode no

                                    $barcode_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";

                                    $barcode_no_results = mysqli_query($db_connection, $barcode_no_query);

                                    $barcode_no_row = $barcode_no_results->fetch_assoc();



                                    echo '

                                        <tr>

                                            <td>' . $active_requests_row["IDRequest"] . '</td>

                                            <td>' . $active_requests_row["RequestNo"] . '</td>

                                            <td>' . $barcode_no_row["BarcodeNo"] . '</td>

                                            <td>' . $active_requests_row["RequestDate"] . '</td>



                                            <td><button class="btn btn-outline-success" type="submit" name="AcceptRequest" value="' . $active_requests_row["RequestNo"] . '">✓</button></td>

                                            <td><button class="btn btn-outline-danger" type="submit" name="CancelRequest" value="' . $active_requests_row["RequestNo"] . '">✘</button></td>

                                        <tr>

                                    ';

                                }



                                echo '<tbody></table>';

                            }

                            else

                            {

                                echo '<div class="text-center"><h4>Wow!</h4>No active requests!</div>';

                            }

                        ?>

                    </form>

                </div>

            </div>

        </div>



        <div class="container">

            <div class="card">

                <div class="card-header">

                    <h6>Pending Requests</h6>

                </div>

                <div class="card-body">

                    <form action="" method="POST">                       

                        <?php

                            if ($pending_requests_results->num_rows > 0)

                            {

                                echo '

                                    <table class="table table-striped table-hover">

                                        <thead class="thead-dark">

                                            <tr>

                                                <th scope ="col">ID Request</th>

                                                <th scope="col">Request No.</th>

                                                <th scope="col">Media No.</th>

                                                <th scope="col">Request Date</th>

                                                <th scope="col">Ready Date</th>

                                                <th scope="col">Expire Date</th>

                                                <th scope="col">Pick Up</th>

                                                <th scope="col">Cancel</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                ';



                                while ($pending_requests_row = $pending_requests_results->fetch_assoc())

                                {

                                    // gets media ID

                                    $IDMedia = $pending_requests_row["MEDIA_IDMedia"];



                                    // query for media barcode no

                                    $barcode_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";

                                    $barcode_no_results = mysqli_query($db_connection, $barcode_no_query);

                                    $barcode_no_row = $barcode_no_results->fetch_assoc();



                                    echo '

                                        <tr>

                                            <td>' . $pending_requests_row["IDRequest"] . '</td>

                                            <td>' . $pending_requests_row["RequestNo"] . '</td>

                                            <td>' . $barcode_no_row["BarcodeNo"] . '</td>

                                            <td>' . $pending_requests_row["RequestDate"] . '</td>

                                            <td>' . $pending_requests_row["ReadyDate"] . '</td>

                                            <td>' . $pending_requests_row["ExpireDate"] . '</td>



                                            <td><button class="btn btn-outline-info" type="submit" name="Pick_Up" value="' . $pending_requests_row["RequestNo"] . '">✓</button></td>

                                            <td><button class="btn btn-outline-danger" type="submit" name="CancelRequest" value="' . $pending_requests_row["RequestNo"] . '">✘</button></td>

                                        <tr>

                                    ';

                                }



                                echo '<tbody></table>';

                            }

                            else

                            {

                                echo '<div class="text-center"><h4>Wow!</h4>No pending requests!</div>';

                            }

                        ?>

                    </form>

                </div>

            </div>

        </div>

        <div class="container">
            <div class="accordion" id="accordion">
                <div class="card">
                    <div class="card-header">
                        <h6 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-past">
                                Past Requests
                            </button>
                        </h6>
                    </div>
                    <div id="collapse-past" class="collapse" data-parent="#accordion">
                        <div class="card-body">                       
                            <?php
                                if ($past_requests_results->num_rows > 0)
                                {
                                    echo '
                                        <table class="table table-small table-striped table-hover">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Request No.</th>
                                                    <th scope="col">Media No.</th>
                                                    <th scope="col">Request Date</th>
                                                    <th scope="col">Ready Date</th>
                                                    <th scope="col">Expire Date</th>
                                                    <th scope="col">Pickup Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';

                                    while ($past_request_row = $past_requests_results->fetch_assoc())
                                    {
                                        // gets media ID
                                        $IDMedia = $past_request_row["MEDIA_IDMedia"];
    
                                        // query for media barcode no
                                        $barcode_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                        $barcode_no_results = mysqli_query($db_connection, $barcode_no_query);
                                        $barcode_no_row = $barcode_no_results->fetch_assoc();

                                        echo '
                                            <tr>
                                                <td>' . $past_request_row["RequestNo"] . '</td>
                                                <td>' . $barcode_no_row["BarcodeNo"] . '</td>
                                                <td>' . $past_request_row["RequestDate"] . '</td>
                                                <td>' . $past_request_row["ReadyDate"] . '</td>
                                                <td>' . $past_request_row["ExpireDate"] . '</td>
                                                <td>' . $past_request_row["PickUpDate"] . '</td>
                                            <tr>
                                        ';
                                    }

                                    echo '<tbody></table>';
                                }
                                else
                                {
                                    echo '<div class="text-center"><h4>Wow!</h4>No past requests!</div>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>





    </body>



    <footer>



    </footer>

</html>