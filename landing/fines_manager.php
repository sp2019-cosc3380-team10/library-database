<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/admin_check.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Fines Manager</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            // includes navbar.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>
    </body>

    <footer>

    </footer>
</html>
