<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/admin_check.php");

    // includes credentials
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

    // includes register special script
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/register_special.php");

    // connects to database
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die ("Unable to connect to database! " . mysqli_connect.error());

    // displays "Select User" card
    $select_user = "display: none;";
    // displays "Modify User" card
    $modify_user = "display: none;";
    // displays deactivation alert
    $deactivate_alert = "";
    // displays reactivation alert
    $reactivate_alert = "";

    if (isset($_GET["SearchBy"]) && isset($_GET["SearchKey"]))
    {
        $select_user = "";
        $SearchBy = $_GET["SearchBy"];
        $SearchKey = $_GET["SearchKey"];
        
        // searches search by similar to search key
        $search_query = "SELECT `LibraryNo`, `UserName`, `FirstName`, `LastName` FROM `USERS` WHERE $SearchBy LIKE '%$SearchKey%' ORDER BY `$SearchBy`;";
    
        // gets search results
        $search_results = mysqli_query($db_connection, $search_query) or die ("Unable to search!");
    }
    
    if (isset($_POST["DeactivateUser"]))
    {
        // sets deactivate alert
        $deactivate_alert = '<div class="alert alert-danger"><strong>Success:</strong> User account deactivated.</div>';

        // grabs GET variable
        $deactivate_user = $_POST["DeactivateUser"];

        // query to deactivate user
        $deactivate_query = "UPDATE `USERS` SET `UserActive`=0 WHERE `LibraryNo`='$deactivate_user';";

        // updates user in database
        mysqli_query($db_connection, $deactivate_query) or die ("Unable to deactivate user! " . mysqli_error($db_connection));
    }
    
    if (isset($_POST["ReactivateUser"]))
    {
        // sets activate alert
        $reactivate_alert = '<div class="alert alert-success"><strong>Success:</strong> User account reactivated.</div>';

        // grabs GET variable
        $reactivate_user = $_POST["ReactivateUser"];

        // query to deactivate user
        $reactivate_query = "UPDATE `USERS` SET `UserActive`=1 WHERE `LibraryNo`='$reactivate_user';";

        // updates user in database
        mysqli_query($db_connection, $reactivate_query) or die ("Unable to deactivate user! " . mysqli_error($db_connection));
    }
    
    if (isset($_GET["ModifyUser"]))
    {
        $modify_user = "";
        $ModifyUser = $_GET["ModifyUser"];

        /**
         * GRABS USER INFO
         */

        // user info query
        $user_info_query = "SELECT `LibraryNo`, `UserName`, `FirstName`, `LastName`, `DateOfBirth`, `UserActive`, `DateOfBirth`, `USER_TYPES_UserTypeID` FROM `USERS` WHERE `LibraryNo`='$ModifyUser';";

        // queries results of user info
        $user_info_results = mysqli_query($db_connection, $user_info_query) or die ("Unable to query user info! " . mysqli_error($db_connection));

        // number of user info rows
        $num_user_info_rows = mysqli_num_rows($user_info_results);

        // if more than 1 row, grab row
        if ($num_user_info_rows > 0)
        {
            // grab user info row
            $user_info_row = $user_info_results->fetch_assoc();
        }
        
        /**
         * GRABS USER TYPE
         */

        // grabs user type ID
        $user_type_id = $user_info_row["USER_TYPES_UserTypeID"];

        // user type query
        $user_type_query = "SELECT `UserType` FROM `USER_TYPES` WHERE `IDUserType`='$user_type_id';";
        
        // queries results of user type
        $user_type_results = mysqli_query($db_connection, $user_type_query) or die ("Unable to query user type! " . mysqli_error($db_connection));

        // number of user info rows
        $num_user_type_rows = mysqli_num_rows($user_info_results);

        // if more than 1 row, grab row
        if ($num_user_type_rows > 0)
        {
            // grabs user type row
            $user_type_row = $user_type_results->fetch_assoc();

        }
    }
    
    mysqli_close($db_connection);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - User Manager</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>

        

        <!-- Custom JavaScript --> 
        <script src="/scripts/validation.js"></script>
    </head>

    <body>
        <?php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Search By User</h6>
                </div>
                <div class="card-body">
                    <form method="get" action="">
                        <div class="row">
                            <div class="col-md-3">
                                <select name="SearchBy" id="search-by" class="custom-select" required>
                                    <option value="" hidden selected>Search user by...</option>
                                    <option value="LibraryNo">Library Number</option>
                                    <option value="UserName">Username</option>
                                    <option value="FirstName">First Name</option>
                                    <option value="LastName">Last Name</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input name="SearchKey" id="search-key" type="text" class="form-control" placeholder="Enter a search key..." required>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="Search" type="text" value="true" style="display: none;">
                    </form>
                </div>
            </div>
        </div>

        <div class="container" style="<?php echo $select_user ?>">
            <div class="card">
                <div class="card-header">
                    <h6>Select User</h6>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <?php
                            // if GET parameters are set
                            if (isset($_GET["SearchBy"]) && isset($_GET["SearchKey"]))
                            {
                                if ($search_results->num_rows > 0)
                                {
                                    echo '
                                        <table class="table table-hover table-striped">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Select</th>
                                                    <th scope="col">Library No.</th>
                                                    <th scope="col">Username</th>
                                                    <th scope="col">First Name</th>
                                                    <th scope="col">Last Name</th>
                                                </tr>
                                            </thead>
                                    ';
                                    echo '<tbody>';
                                    
                                    // prints out each row in the table
                                    while ($row = $search_results->fetch_assoc())
                                    {
                                        echo '
                                            <tr>
                                                <td><button type="submit" name="ModifyUser" value="' . $row["LibraryNo"] . '" class="btn btn-outline-secondary">+</button></td>
                                                <td>' . $row["LibraryNo"] . '</td>
                                                <td>' . $row["UserName"] . '</td>
                                                <td>' . $row["FirstName"] . '</td>
                                                <td>' . $row["LastName"] . '</td>
                                            </tr>
                                        ';
                                    }

                                    
                                    echo '</tbody></table>';
                                }
                                else
                                {
                                    echo '<div class="text-center"><h4>No search results...</h4></div>';
                                }
                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

        <div class="container" style="<?php echo $modify_user ?>">
            <div class="card">
                <div class="card-header">
                    <h6>Modify: <?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["LastName"] . ", " . $user_info_row["FirstName"] . " [" . $user_info_row["UserName"] . ", " . $user_info_row["LibraryNo"] . "]"; } ?></h6>
                </div>
                <div class="card-body">
                    <?php
                        echo $deactivate_alert;
                        echo $reactivate_alert;
                    ?>
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Library Number</h4>
                            <?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["LibraryNo"]; } ?>
                        </div>
                        <div class="col-md-4">
                            <h4>Account Type</h4>
                            <?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["UserName"]; } ?>
                        </div>
                        <div class="col-md-4">
                            <h4>User Type</h4>
                            <?php if(isset($_GET["ModifyUser"])) { echo ucfirst($user_type_row["UserType"]); } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <h4>First Name</h4>
                            <?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["FirstName"]; } ?>
                        </div>
                        <div class="col-md-4">
                            <h4>Last Name</h4>
                            <?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["LastName"]; } ?>
                        </div>
                        <div class="col-md-4">
                            <h4>Date of Birth</h4>
                            <?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["DateOfBirth"]; } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Account Active</h4>
                            <?php
                                if (isset($_GET["ModifyUser"]))
                                {
                                    if ($user_info_row["UserActive"] == 1)
                                    {
                                        echo "Yes";
                                    }
                                    else
                                    {
                                        echo "No";
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="container text-center">
                    <form action="" method="POST">
                        <button class="btn btn-outline-danger" type="submit" name="DeactivateUser" value="<?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["LibraryNo"]; } ?>">Deactivate Account</button>
                        <button class="btn btn-outline-success" type="submit" name="ReactivateUser" value="<?php if(isset($_GET["ModifyUser"])) { echo $user_info_row["LibraryNo"]; } ?>">Reactivate Account</button>
                    </form>
                </div>
            </div>
        </div>


        <div class="container">
            <div id="accordion">
                <div class="card">
                    <div class="card-header">
                        <h6 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne">
                                Add Special User
                            </button>
                        </h6>
                    </div>
                    <div id="collapseOne" class="collapse <?php echo $open_register ?>" data-parent="#accordion">
                        <div class="card-body">
                            <?php
                                echo $register_messages;
                            ?>
                            <form action="" method="POST">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label>User Type</label>
                                            <select name="register-usertype" class="custom-select" required>
                                                <option value="" hidden selected>Select a user type...</option>
                                                <option value="admin">Admin</option>
                                                <option value="staff">Staff</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <label for="register-username">Username</label>
                                            <input type="text" class="form-control" id="register-username" name="register-username" placeholder="Username" maxlength="20" required>
                                        </div>
        
                                    </div>
                                    <div class="form-group">
                                        <label for="register-email">Email</label>
                                        <input type="email" class="form-control" id="register-email" name="register-email" placeholder="Email" maxlength="320" required>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="register-firstname">First Name</label>
                                            <input type="text" class="form-control" id="register-firstname" name="register-firstname" placeholder="First Name" maxlength="50" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="register-lastname">Last Name</label>
                                            <input type="text" class="form-control" id="register-lastname" name="register-lastname" placeholder="Last Name" maxlength="50" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="register-birthdate">Birthday</label>
                                        <input type="date" class="form-control" id="register-birthdate" name="register-birthdate" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="register-address">Street Address</label>
                                        <input type="address" class="form-control" id="register-address" name="register-address" placeholder="Street Address" maxlength="100" required>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-5">
                                            <label for="register-city">City</label>
                                            <input type="city" class="form-control" id="register-city" name="register-city" placeholder="City" maxlength="50" required>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="register-zip">Zip</label>
                                            <input type="text" class="form-control" id="register-zip" name="register-zip" placeholder="Zip" maxlength="5" required>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label for="register-state">State</label>
                                            <select class="form-control" id="register-state" name="register-state" required>
                                                <option selected hidden disabled>Select a state...</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="container text-center">
                                    <button type="submit" name="register" class="btn btn-primary">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

    <footer>

    </footer>
</html>
