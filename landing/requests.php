<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");

    // includes db credentials
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

    // establishes connection with the database
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    // cancel request message
    $CancelRequestMessage = "";
    // if cancel request
    if (isset($_POST["CancelRequest"]))
    {
        // gets request number
        $RequestNo = $_POST["CancelRequest"];

        // query update to the request
        $request_cancel_query = "UPDATE `REQUESTS` SET `CancelDate`=NOW(), `RequestActive`=0 WHERE `RequestNo`='$RequestNo';";
        mysqli_query($db_connection, $request_cancel_query);

        $CancelRequestMessage = '<div class="container"><div class="alert alert-danger"><strong>Canceled:</strong> The request ' . $RequestNo . ' has been canceled.</div></div>';
    }

    $NewRequestMessage = "";
    // if barcode no set for request
    if (isset($_POST["BarcodeNo"]))
    {
        $BarcodeNo = $_POST["BarcodeNo"];
        $UserType = $_SESSION["UserType"];
        $LibraryNo = $_SESSION["LibraryNo"];

        // request limit query
        $request_limit_query = "SELECT `RequestLimit` FROM `USER_TYPES` WHERE `UserType`='$UserType';";
        $request_limit_results = mysqli_query($db_connection, $request_limit_query);
        $request_limit_row = $request_limit_results->fetch_assoc();
        $RequestLimit = $request_limit_row["RequestLimit"];

        // grabs the number fo active 
        $request_count_query = "SELECT COUNT(*) FROM `REQUESTS` WHERE `USERS_IDUser`=(SELECT IDUser FROM `USERS` WHERE `LibraryNo`='$LibraryNo') AND `RequestActive`=1;";        
        $request_count_results = mysqli_query($db_connection, $request_count_query);
        $request_limit_row = $request_count_results->fetch_assoc();
        $RequestCount = $request_limit_row["COUNT(*)"];

        // if the user is at or past the request limit, restrict from being able to make a request
        if ($RequestCount >= $RequestLimit)
        {
            $NewRequestMessage = '<div class="container"><div class="alert alert-danger"><strong>Error:</strong> Unable to make a request. You have reached your request limit.</div></div>';
        }
        // else create a new request for the user
        else
        {
            // queries for associated ID user
            $id_user_query = "SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo';";
            $id_user_results = mysqli_query($db_connection, $id_user_query);
            $id_user_row = $id_user_results->fetch_assoc();
            $IDUser = $id_user_row["IDUser"];

            // queries for associated ID media
            $id_media_query = "SELECT `IDMedia` FROM `MEDIA` WHERE `BarcodeNo`='$BarcodeNo';";
            $id_media_results = mysqli_query($db_connection, $id_media_query);
            $id_media_row = $id_media_results->fetch_assoc();
            $IDMedia = $id_media_row["IDMedia"];

            // continuously generate a request number until unique
            do {
                // generates a request number
                $RequestNo = substr(hash("sha512", $LibraryNo . $BarcodeNo), 0 , 8);

                // query to find duplicate request numbers
                $unique_requestno_query = "SELECT `RequestNo` FROM `REQUESTS` WHERE `RequestNo`='$RequestNo';";
                
                // queries database
                $unique_requestno_results = mysqli_query($db_connection, $unique_requestno_query);

            } while($unique_requestno_results->num_rows > 0);

            $new_request_query = "INSERT INTO `REQUESTS` (`RequestNo`, `USERS_IDUser`, `MEDIA_IDMedia`, `RequestDate`) VALUES ('$RequestNo', '$IDUser', '$IDMedia', NOW());";
            mysqli_query($db_connection, $new_request_query) or die ("Unable to insert a new request!");

            $NewRequestMessage = '<div class="container"><div class="alert alert-success"><strong>Success:</strong> Request #' . $RequestNo . ' has been created.</div></div>';
        }
    }

    // gets library number from session
    $LibraryNo = $_SESSION["LibraryNo"];

    // queries for active and past requests
    $active_requests_query = "SELECT * FROM `REQUESTS` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo' AND `RequestActive`='1');";
    $past_requests_query = "SELECT * FROM `REQUESTS` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo' AND `RequestActive`='0');";

    // queries statements into database
    $active_requests_results = mysqli_query($db_connection, $active_requests_query);
    $past_requests_results = mysqli_query($db_connection, $past_requests_query);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Requests</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            // includes navbar.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");

            echo $CancelRequestMessage;
            echo $NewRequestMessage;
        ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    Create New Request
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <select>
                                
                            </select>
                        </div>
                        <div class="col-md-8">
                        </div>
                    </div>
                </div>
            </div>  
        </div>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Active Requests</h6>
                </div>
                <div class="card-body">
                    <form action="" method="POST">                       
                        <?php
                            if ($active_requests_results->num_rows > 0)
                            {
                                echo '
                                    <table class="table table-striped table-hover">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">Request No.</th>
                                                <th scope="col">Request Date</th>
                                                <th scope="col">Media No.</th>
                                                <th scope="col">Ready Date</th>
                                                <th scope="col">Expire Date</th>
                                                <th scope="col">Cancel</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                ';

                                while ($active_requests_row = $active_requests_results->fetch_assoc())
                                {
                                    // gets media ID
                                    $IDMedia = $active_requests_row["MEDIA_IDMedia"];

                                    // query for media barcode no
                                    $barcode_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                    $barcode_no_results = mysqli_query($db_connection, $barcode_no_query);
                                    $barcode_no_row = $barcode_no_results->fetch_assoc();

                                    echo '
                                        <tr>
                                            <td>' . $active_requests_row["RequestNo"] . '</td>
                                            <td>' . $active_requests_row["RequestDate"] . '</td>
                                            <td>' . $barcode_no_row["BarcodeNo"] . '</td>
                                            <td>' . $active_requests_row["ReadyDate"] . '</td>
                                            <td>' . $active_requests_row["ExpireDate"] . '</td>
                                            <td><button class="btn btn-outline-danger" type="submit" name="CancelRequest" value="' . $active_requests_row["RequestNo"] . '">X</button></td>
                                        <tr>
                                    ';
                                }

                                echo '<tbody></table>';
                            }
                            else
                            {
                                echo '<div class="text-center"><h4>Wow!</h4>No active requests!</div>';
                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="accordion" id="accordion">
                <div class="card">
                    <div class="card-header">
                        <h6 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-past">
                                Past Requests
                            </button>
                        </h6>
                    </div>
                    <div id="collapse-past" class="collapse" data-parent="#accordion">
                        <div class="card-body">                       
                            <?php
                                if ($past_requests_results->num_rows > 0)
                                {
                                    echo '
                                        <table class="table table-small table-striped table-hover">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Request No.</th>
                                                    <th scope="col">Request Date</th>
                                                    <th scope="col">Media No.</th>
                                                    <th scope="col">Ready Date</th>
                                                    <th scope="col">Expire Date</th>
                                                    <th scope="col">Pickup Date</th>
                                                    <th scope="col">Cancel Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';

                                    while ($past_request_row = $past_requests_results->fetch_assoc())
                                    {
                                        // gets media ID
                                        $IDMedia = $past_request_row["MEDIA_IDMedia"];
    
                                        // query for media barcode no
                                        $barcode_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                        $barcode_no_results = mysqli_query($db_connection, $barcode_no_query);
                                        $barcode_no_row = $barcode_no_results->fetch_assoc();

                                        echo '
                                            <tr>
                                                <td>' . $past_request_row["RequestNo"] . '</td>
                                                <td>' . $past_request_row["RequestDate"] . '</td>
                                                <td>' . $barcode_no_row["BarcodeNo"] . '</td>
                                                <td>' . $past_request_row["ReadyDate"] . '</td>
                                                <td>' . $past_request_row["ExpireDate"] . '</td>
                                                <td>' . $past_request_row["PickUpDate"] . '</td>
                                                <td>' . $past_request_row["CancelDate"] . '</td>
                                            <tr>
                                        ';
                                    }

                                    echo '<tbody></table>';
                                }
                                else
                                {
                                    echo '<div class="text-center"><h4>Wow!</h4>No past requests!</div>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <footer>

    </footer>
</html>
