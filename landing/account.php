<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

    // connects to database
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die ("Unable to connect to MySQL! " . mysqli_connect_error());

    // stores session libraryno to a variable
    $LibraryNo = $_SESSION["LibraryNo"];

    /**
     * STEP 1: GET USER INFO
     */

    // query for getting user info
    $user_info_query = "SELECT `IDUser`, LibraryNo, UserName, FirstName, LastName, DateOfBirth, CONTACTS_ContactID FROM `USERS` WHERE LibraryNo='$LibraryNo';";
    
    // queries user info
    $user_info_results = mysqli_query($db_connection, $user_info_query) or die("Unable to query user info!" . mysqli_error($db_connection));

    // fetches associative array of user results
    $user_row = mysqli_fetch_assoc($user_info_results);

    // stores user info row data into variables
    $IDUser = $user_row["IDUser"];
    $LibraryNo = $user_row["LibraryNo"];
    $UserName = $user_row["UserName"];
    $FirstName = $user_row["FirstName"];
    $LastName = $user_row["LastName"];
    $DateOfBirth = $user_row["DateOfBirth"];
    $CONTACTS_ContactID = $user_row["CONTACTS_ContactID"];

    /**
     * STEP 2: GET CONTACT INFO
     */

    // query for getting contact info
    $contact_info_query = "SELECT Email, PhoneNo, StreetAddress, City, State, ZipCode FROM `CONTACTS` WHERE 
    IDContact=(SELECT CONTACTS_ContactID FROM `USERS` WHERE LibraryNo='$LibraryNo')";

    // queries contact info
    $contact_info_results = mysqli_query($db_connection, $contact_info_query) or die("Unable to query contact info!" . mysqli_error($db_connection));

    // fetches associative array of contact results
    $contact_row = mysqli_fetch_assoc($contact_info_results);

    // stores contact info row data into variables
    $Email = $contact_row["Email"];
    $PhoneNo = $contact_row["PhoneNo"];
    $StreetAddress = $contact_row["StreetAddress"];
    $City = $contact_row["City"];
    $State = $contact_row["State"];
    $ZipCode = $contact_row["ZipCode"];

    $PasswordChangedMessage = "";
    if (isset($_POST["new-password"]) && isset($_POST["new-confirm"]))
    {
        if ($_POST["new-password"] != $_POST["new-confirm"])
        {
            $PasswordChangedMessage = '<div class="container"><div class="alert alert-danger"><strong>Error:</strong> Unable to change passwords. Passwords are not matching.</div></div>';
        }
        else
        {
            // hashes the password
            $Password = hash("sha512", $_POST["new-password"]);

            $LibraryNo = $_SESSION["LibraryNo"];
            
            // queries for associated ID user
            $id_user_query = "SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo';";
            $id_user_results = mysqli_query($db_connection, $id_user_query);
            $id_user_row = $id_user_results->fetch_assoc();
            $IDUser = $id_user_row["IDUser"];

            // queries for ID login
            $id_login_query = "SELECT `IDLogin` FROM `LOGINS`, `USERS` WHERE `IDLogin`=`LOGINS_IDLogin` AND `IDUser`='$IDUser';";
            $id_login_results = mysqli_query($db_connection, $id_login_query);
            $id_login_row = $id_login_results->fetch_assoc();
            $IDLogin = $id_login_row["IDLogin"];

            // queries to update login password
            $pass_update_query = "UPDATE `LOGINS` SET `Password`='$Password' WHERE `IDLogin`='$IDLogin';";
            mysqli_query($db_connection, $pass_update_query) or die ("Unable to change password!");

            $PasswordChangedMessage = '<div class="container"><div class="alert alert-success"><strong>Success:</strong> Password changed.</div></div>';
        }
    }

    $AccountInfoMessage = "";
    if (isset($_POST["new-account"]))
    {
        $NewFirst = $_POST["new-first"];
        $NewLast = $_POST["new-last"];
        $NewBirth = $_POST["new-birth"];

        $update_user_query = "UPDATE `USERS` SET `FirstName`='$NewFirst', `LastName`='$NewLast', `DateOfBirth`='$NewBirth' WHERE `IDUser`='$IDUser' OR `LibraryNo`='$LibraryNo';";
        mysqli_query($db_connection, $update_user_query);

        $AccountInfoMessage = '<div class="container"><div class="alert alert-success"><strong>Success:</strong> Account information changed.</div></div>';
    }

    $ContactInfoMessage = "";
    if (isset($_POST["new-contact"]))
    {
        $NewEmail = $_POST["new-email"];
        $NewPhone = $_POST["new-phone"];
        $NewAddress = $_POST["new-address"];
        $NewCity = $_POST["new-city"];
        $NewState = $_POST["new-state"];
        $NewZip = $_POST["new-zip"];

        $update_contact_query = "UPDATE `CONTACTS` SET `Email`='$NewEmail', `PhoneNo`='$NewPhone', `StreetAddress`='$NewAddress', `City`='$NewCity', `State`='$NewState', `ZipCode`='$NewZip' WHERE `IDContact`='$CONTACTS_ContactID';";

        mysqli_query($db_connection, $update_contact_query);

        $ContactInfoMessage = '<div class="container"><div class="alert alert-success"><strong>Success:</strong> Contact information changed.</div></div>';
    }


    

    /**
     * GET USER INFO AGAIN
     */

    // query for getting user info
    $user_info_query = "SELECT `IDUser`, LibraryNo, UserName, FirstName, LastName, DateOfBirth, CONTACTS_ContactID FROM `USERS` WHERE LibraryNo='$LibraryNo';";
    
    // queries user info
    $user_info_results = mysqli_query($db_connection, $user_info_query) or die("Unable to query user info!" . mysqli_error($db_connection));

    // fetches associative array of user results
    $user_row = mysqli_fetch_assoc($user_info_results);

    // stores user info row data into variables
    $IDUser = $user_row["IDUser"];
    $LibraryNo = $user_row["LibraryNo"];
    $UserName = $user_row["UserName"];
    $FirstName = $user_row["FirstName"];
    $LastName = $user_row["LastName"];
    $DateOfBirth = $user_row["DateOfBirth"];

    /**
     * STEP 2: GET CONTACT INFO
     */

    // query for getting contact info
    $contact_info_query = "SELECT Email, PhoneNo, StreetAddress, City, State, ZipCode FROM `CONTACTS` WHERE 
    IDContact=(SELECT CONTACTS_ContactID FROM `USERS` WHERE LibraryNo='$LibraryNo')";

    // queries contact info
    $contact_info_results = mysqli_query($db_connection, $contact_info_query) or die("Unable to query contact info!" . mysqli_error($db_connection));

    // fetches associative array of contact results
    $contact_row = mysqli_fetch_assoc($contact_info_results);

    // stores contact info row data into variables
    $Email = $contact_row["Email"];
    $PhoneNo = $contact_row["PhoneNo"];
    $StreetAddress = $contact_row["StreetAddress"];
    $City = $contact_row["City"];
    $State = $contact_row["State"];
    $ZipCode = $contact_row["ZipCode"];




    mysqli_close($db_connection);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Account</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");

            echo $PasswordChangedMessage;
            echo $AccountInfoMessage;
            echo $ContactInfoMessage;
        ?>

        <div class="container">
            <div class="card card-body">
                <h2>Your Account Information</h2>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <h4>Library Number</h4> <?php echo $LibraryNo; ?>
                    </div>
                    <div class="col-md-4">
                        <h4>Account Type</h4> <?php echo ucfirst($_SESSION["UserType"]); ?>
                    </div>
                    <div class="col-md-4">
                        <h4>Username</h4> <?php echo $UserName; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4>First Name</h4> <?php echo $FirstName; ?>
                    </div>
                    <div class="col-md-4">
                        <h4>Last Name</h4> <?php echo $LastName; ?>
                    </div>
                    <div class="col-md-4">
                        <h4>Date of Birth</h4> <?php echo $DateOfBirth; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="card card-body">
                <h2>Your Contact Information</h2>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Email</h4> <?php echo $Email; ?>
                    </div>
                    <div class="col-md-6">
                        <h4>Phone Number</h4> <?php echo $PhoneNo; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Street Address</h4> <?php echo $StreetAddress; ?>
                    </div>
                    <div class="col-md-6">
                        <h4>City</h4> <?php echo $City; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4>State</h4> <?php echo $State; ?>
                    </div>
                    <div class="col-md-6">
                        <h4>Zip Code</h4> <?php echo $ZipCode; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Change Password</h6>
                </div>
                <div class="card-body">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="new-password">New Password</label>
                            <input type="password" id="new-password" name="new-password" class="form-control" minlength="10" maxlength="128" required>
                        </div>
                        <div class="form-group">
                            <label for="new-confirm">Confirm New Password</label>
                            <input type="password" id="new-confirm" name="new-confirm" class="form-control" minlength="10" maxlength="128" required>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-primary" type="submit" name="PasswordChangeBtn">Change Password</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Change Account Information</h6>
                </div>
                <div class="card card-body">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="new-first">New First Name</label>
                            <input type="text" id="new-first" name="new-first" class="form-control" maxlength="50" value="<?php echo $FirstName; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="new-last">New Last Name</label>
                            <input type="text" id="new-last" name="new-last" class="form-control" maxlength="50" value="<?php echo $LastName; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="new-birth">New Date of Birth</label>
                            <input type="date" id="new-birth" name="new-birth" class="form-control" value="<?php echo $DateOfBirth ?>" required>
                        </div>
                        <div class="text-center">
                            <button type="submit" name="new-account" class="btn btn-primary">Change Contact Info</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Change Contact Information</h6>
                </div>
                <div class="card-body">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label>New Email</label>
                            <input type="email" id="new-email" name="new-email" class="form-control" maxlength="320" value="<?php echo $Email; ?>">
                        </div>
                        <div class="form-group">
                            <label>New Phone Number</label>
                            <input type="text" id="new-phone" name="new-phone" class="form-control" maxlength="15"  value="<?php echo $PhoneNo; ?>">
                        </div>
                        <div class="form-group">
                            <label>New Street Address</label>
                            <input type="text" id="new-address" name="new-address" class="form-control" maxlength="100" value="<?php echo $StreetAddress; ?>">
                        </div>
                        <div class="form-group">
                            <label>New City</label>
                            <input type="text" id="new-city" name="new-city" class="form-control" maxlength="50" value="<?php echo $City; ?>">
                        </div>
                        <div class="form-group">
                            <label>New State</label>
                            <select class="form-control" id="new-state" name="new-state" required>
                                <option value="" hidden disabled selected>Select a state...</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>New Zip Code</label>
                            <input type="text" id="new-zip" name="new-zip" class="form-control" minlength="5" maxlength="5" value="<?php echo $ZipCode; ?>" required>
                        </div>
                        <div class="text-center">
                            <button type="submit" name="new-contact" class="btn btn-primary">Change Contact Info</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </body>

    <footer>

    </footer>
</html>
