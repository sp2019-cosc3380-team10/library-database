<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/admin_check.php");
    $usernumber_error = "";
    $itemnumber_error = "";
    $checkout_success_message = "";
    $checkin_success_message = "";
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/checkout.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/checkin.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Checkout/Return</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>
	
	<div class="container">
	    <div class="card card-body">
	        <h2> Checkout / Return an item: </h2>
		<hr>
		<form action="" method="POST">
			<div class="row">
				<div class="col-md-6">
					<div class="modal-body">
						<h4>Checkout</h4>
						<div class="form-group" name="checkout">
							<label for="checkout-usernumber">Loaning to user #</label>
							<input type="text" class="form-control" id="checkout-usernumber" name="checkout-usernumber" placeholder="User #">
							<label for="checkout-medianumber">Media # to check out</label>
							<input type="text" class="form-control" id="checkout-medianumber" name="checkout-medianumber" placeholder="Media #">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="modal-body">
						<h4>Return</h4>
						<br>
						<div class="form-group" name="checkin">
							<label for="checkin-loannumber">Loan # to check in</label>
							<input type="text" class="form-control" id="checkin-loannumber" name="checkin-loannumber" placeholder="Loan #">
						</div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="modal-footer">
						<button name="checkout" type="submit" class="btn btn-secondary">Checkout</button>
					</div>
				</div>
				<div class="col-md-6">
					<div class="modal-footer">
						<button name="checkin" type="submit" class="btn btn-secondary" >Return</button>
					</div>
				</div>
				<br>
				<p class="text-info">
					<?php echo $checkout_success_message . $checkin_success_message; ?>
				</p>
				<p class="text-danger">
					<?php echo $usernumber_error . " " . $itemnumber_error; ?>
				</p>
			</div>
		</form>
	    </div>
	</div>
	
    </body>

    <footer>

    </footer>
</html>
