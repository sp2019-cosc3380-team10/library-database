<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/admin_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

    // establishes connection to database
    $db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Statistics</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>

        <script src="/chart/Chart.bundle.min.js"></script>
    </head>

    <body>
        <?php
            // includes navbar.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Users Statistics</h6>
                </div>
                <div class="card-body">

                    <?php
                        // active users
                        $active_users_query = "SELECT COUNT(*) FROM `USERS` WHERE `UserActive`='1';";
                        $active_users_results = mysqli_query($db_connection, $active_users_query);
                        $active_users_row = $active_users_results->fetch_assoc();

                        // deactivated users
                        $deactivated_users_query = "SELECT COUNT(*) FROM `USERS` WHERE `UserActive`='0';";
                        $deactivated_users_results = mysqli_query($db_connection, $deactivated_users_query);
                        $deactivated_users_row = $deactivated_users_results->fetch_assoc();

                        // student users
                        $student_type_query = "SELECT COUNT(*) FROM `USERS` WHERE `USER_TYPES_UserTypeID`=(SELECT `IDUserType` FROM `USER_TYPES` WHERE `UserType`='student') AND `UserActive`='1';";
                        $student_type_results = mysqli_query($db_connection, $student_type_query);
                        $student_type_row = $student_type_results->fetch_assoc();

                        // staff users
                        $staff_type_query = "SELECT COUNT(*) FROM `USERS` WHERE `USER_TYPES_UserTypeID`=(SELECT `IDUserType` FROM `USER_TYPES` WHERE `UserType`='staff') AND `UserActive`='1';";
                        $staff_type_results = mysqli_query($db_connection, $staff_type_query);
                        $staff_type_row = $staff_type_results->fetch_assoc();
                        
                        // admin users
                        $admin_type_query = "SELECT COUNT(*) FROM `USERS` WHERE `USER_TYPES_UserTypeID`=(SELECT `IDUserType` FROM `USER_TYPES` WHERE `UserType`='admin') AND `UserActive`='1';";
                        $admin_type_results = mysqli_query($db_connection, $admin_type_query);
                        $admin_type_row = $admin_type_results->fetch_assoc();
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <!-- User account status -->
                            <h6 class="text-center">Total Users</h6>
                            <canvas id="UsersActive" height="200px"></canvas>
                            <script type="text/javascript">
                                var ctx = document.getElementById('UsersActive').getContext('2d');
                                var myPieChart = new Chart(ctx, {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                <?php echo $active_users_row["COUNT(*)"]; ?>,
                                                <?php echo $deactivated_users_row["COUNT(*)"]; ?>
                                            ],

                                            backgroundColor: [
                                                '#BAFFC9',
                                                '#C6CCCC'
                                            ]
                                            
                                        }],
                                        
                                        labels: [
                                            "Activated",
                                            "Deactivated"
                                        ]

                                    }
                                });
                            </script>
                        </div>
                        
                        <div class="col-md-6">
                            <!-- User types break down -->
                            <h6 class="text-center">User Types</h6>
                            <canvas id="Users" height="200px"></canvas>
                            <script type="text/javascript">
                                var ctx = document.getElementById('Users').getContext('2d');
                                var myPieChart = new Chart(ctx, {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                <?php echo $admin_type_row["COUNT(*)"]; ?>, 
                                                <?php echo $staff_type_row["COUNT(*)"]; ?>,
                                                <?php echo $student_type_row["COUNT(*)"]; ?>,
                                                <?php echo $deactivated_users_row["COUNT(*)"]; ?>
                                            ],

                                            backgroundColor: [
                                                '#FFB3BA',
                                                '#FFDFBA',
                                                '#BAFFC9',
                                                '#C6CCCC'
                                            ]
                                            
                                        }],
                                        
                                        labels: [
                                            "Admins",
                                            "Staffs",
                                            "Students",
                                            "Deactivated"
                                        ]

                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Media Statistics</h6>
                </div>
                <div class="card-body">
                    <?php
                        // total quantity
                        $total_quantity_query = "SELECT SUM(`QuantityTotal`) FROM `MEDIA`;";
                        $total_quantity_results = mysqli_query($db_connection, $total_quantity_query);
                        $total_quantity_row = $total_quantity_results->fetch_assoc();

                        // available quantity
                        $available_quantity_query = "SELECT SUM(`QuantityAvailable`) FROM `MEDIA`;";
                        $available_quantity_results = mysqli_query($db_connection, $available_quantity_query);
                        $available_quantity_row = $available_quantity_results->fetch_assoc();

                        // total books
                        $total_books_query = "SELECT COUNT(*) FROM `MEDIA` WHERE `MEDIA_TYPES_IDMediaType`=(SELECT IDMediaType FROM `MEDIA_TYPES` WHERE `MediaTypeName`='book');";
                        $total_books_results = mysqli_query($db_connection, $total_books_query);
                        $total_books_row = $total_books_results->fetch_assoc();

                        // total films
                        $total_film_query = "SELECT COUNT(*) FROM `MEDIA` WHERE `MEDIA_TYPES_IDMediaType`=(SELECT IDMediaType FROM `MEDIA_TYPES` WHERE `MediaTypeName`='film');";
                        $total_film_results = mysqli_query($db_connection, $total_film_query);
                        $total_film_row = $total_film_results->fetch_assoc();

                        // total cds
                        $total_cd_query = "SELECT COUNT(*) FROM `MEDIA` WHERE `MEDIA_TYPES_IDMediaType`=(SELECT IDMediaType FROM `MEDIA_TYPES` WHERE `MediaTypeName`='cd');";
                        $total_cd_results = mysqli_query($db_connection, $total_cd_query);
                        $total_cd_row = $total_cd_results->fetch_assoc();
                    ?>
                    <div class="row">
                        
                        <div class="col-md-6">
                            <!-- Total media available and checked out  -->
                            <h6 class="text-center">Media Quantity</h6>
                            <canvas id="TotalMedia" height="200px"></canvas>
                            <script type="text/javascript">
                                var ctx = document.getElementById('TotalMedia').getContext('2d');
                                var myPieChart = new Chart(ctx, {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                <?php echo $available_quantity_row["SUM(`QuantityAvailable`)"]; ?>, 
                                                <?php echo $total_quantity_row["SUM(`QuantityTotal`)"] - $available_quantity_row["SUM(`QuantityAvailable`)"]; ?>
                                            ],

                                            backgroundColor: [
                                                '#BAFFC9',
                                                '#FFB3BA'
                                            ]
                                            
                                        }],
                                        
                                        labels: [
                                            "Available",
                                            "Checked Out"
                                        ]

                                    }
                                });
                            </script>
                        </div>
                        
                        <div class="col-md-6">
                            <!-- User types break down -->
                            <h6 class="text-center">Media Types</h6>
                            <canvas id="MediaBreakdown" height="200px"></canvas>
                            <script type="text/javascript">
                                var ctx = document.getElementById('MediaBreakdown').getContext('2d');
                                var myPieChart = new Chart(ctx, {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                <?php echo $total_books_row["COUNT(*)"]; ?>, 
                                                <?php echo $total_film_row["COUNT(*)"]; ?>,
                                                <?php echo $total_cd_row["COUNT(*)"]; ?>
                                            ],

                                            backgroundColor: [
                                                '#FFB3BA',
                                                '#FFDFBA',
                                                '#BAFFC9'
                                            ]
                                            
                                        }],
                                        
                                        labels: [
                                            "Books",
                                            "Films",
                                            "CDs"
                                        ]

                                    }
                                });
                            </script>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Fine Statistics</h6>
                </div>
                <div class="card-body">
                    <?php
                        // total late fines
                        $total_late_fines_query = "SELECT COUNT(*) FROM FINES, FINE_COSTS, FINE_TYPE WHERE FINE_COSTS_IDFineCost=IDFineCost AND FINE_TYPE_IDFineType=(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='late');";
                        $total_late_fines_results = mysqli_query($db_connection, $total_late_fines_query);
                        $total_late_fines_row = $total_late_fines_results->fetch_assoc();

                        // total damage fines
                        $total_damage_fines_query = "SELECT COUNT(*) FROM FINES, FINE_COSTS, FINE_TYPE WHERE FINE_COSTS_IDFineCost=IDFineCost AND FINE_TYPE_IDFineType=(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='damage');";
                        $total_damage_fines_results = mysqli_query($db_connection, $total_damage_fines_query);
                        $total_damage_fines_row = $total_damage_fines_results->fetch_assoc();

                        // total lost fines
                        $total_lost_fines_query = "SELECT COUNT(*) FROM FINES, FINE_COSTS, FINE_TYPE WHERE FINE_COSTS_IDFineCost=IDFineCost AND FINE_TYPE_IDFineType=(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='lost');";
                        $total_lost_fines_results = mysqli_query($db_connection, $total_lost_fines_query);
                        $total_lost_fines_row = $total_lost_fines_results->fetch_assoc();

                        // total late fines costs
                        $total_late_fines_costs_query = "SELECT SUM(`FineCost`) FROM FINES, FINE_COSTS, FINE_TYPE WHERE `FINE_COSTS_IDFineCost`=`IDFineCost` AND FINE_TYPE_IDFineType=(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='late');";
                        $total_late_fines_costs_results = mysqli_query($db_connection, $total_late_fines_costs_query);
                        $total_late_fines_costs_row = $total_late_fines_costs_results->fetch_assoc();
                        if (is_null($total_late_fines_costs_row["SUM(`FineCost`)"]))
                        {
                            $total_late_fines_cost = 0;
                        }
                        else
                        {
                            $total_late_fines_cost = $total_late_fines_costs_row["SUM(`FineCost`)"];
                        }

                        // total damage fines costs
                        $total_damage_fines_costs_query = "SELECT SUM(`FineCost`) FROM FINES, FINE_COSTS, FINE_TYPE WHERE `FINE_COSTS_IDFineCost`=`IDFineCost` AND FINE_TYPE_IDFineType=(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='damage');";
                        $total_damage_fines_costs_results = mysqli_query($db_connection, $total_damage_fines_costs_query);
                        $total_damage_fines_costs_row = $total_damage_fines_costs_results->fetch_assoc();
                        if (is_null($total_damage_fines_costs_row["SUM(`FineCost`)"]))
                        {
                            $total_damage_fines_cost = 0;
                        }
                        else
                        {
                            $total_damage_fines_cost = $total_damage_fines_costs_row["SUM(`FineCost`)"];
                        }
                        
                        // total lost fines cost
                        $total_lost_fines_costs_query = "SELECT SUM(`FineCost`) FROM FINES, FINE_COSTS, FINE_TYPE WHERE `FINE_COSTS_IDFineCost`=`IDFineCost` AND FINE_TYPE_IDFineType=(SELECT IDFineType FROM FINE_TYPE WHERE FineTypeName='lost');";
                        $total_lost_fines_costs_results = mysqli_query($db_connection, $total_lost_fines_costs_query);
                        $total_lost_fines_costs_row = $total_lost_fines_costs_results->fetch_assoc();
                        if (is_null($total_lost_fines_costs_row["SUM(`FineCost`)"]))
                        {
                            $total_lost_fines_cost = 0;
                        }
                        else
                        {
                            $total_lost_fines_cost = $total_lost_fines_costs_row["SUM(`FineCost`)"];
                        }
                    ?>
                    

                    <div class="row">

                        <div class="col-md-6">
                            <!-- User types break down -->
                            <h6 class="text-center">Total Fines Breakdown</h6>
                            <canvas id="TotalFines" height="200px"></canvas>
                            <script type="text/javascript">
                                var ctx = document.getElementById('TotalFines').getContext('2d');
                                var myPieChart = new Chart(ctx, {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                <?php echo $total_late_fines_row["COUNT(*)"]; ?>, 
                                                <?php echo $total_damage_fines_row["COUNT(*)"]; ?>,
                                                <?php echo $total_lost_fines_row["COUNT(*)"]; ?>
                                            ],

                                            backgroundColor: [
                                                '#FFB3BA',
                                                '#FFDFBA',
                                                '#BAFFC9'
                                            ]
                                            
                                        }],
                                        
                                        labels: [
                                            "Late Fines",
                                            "Damage Fines",
                                            "Lost Fines"
                                        ]

                                    }
                                });
                            </script>
                        </div>

                        
                        <div class="col-md-6">
                            <!-- User types break down -->
                            <h6 class="text-center">Total Fine Costs</h6>
                            <canvas id="TotalFineCosts" height="200px"></canvas>
                            <script type="text/javascript">
                                var ctx = document.getElementById('TotalFineCosts').getContext('2d');
                                var myPieChart = new Chart(ctx, {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                <?php echo $total_late_fines_cost; ?>, 
                                                <?php echo $total_damage_fines_cost; ?>,
                                                <?php echo $total_lost_fines_cost; ?>
                                            ],

                                            backgroundColor: [
                                                '#FFB3BA',
                                                '#FFDFBA',
                                                '#BAFFC9'
                                            ]
                                            
                                        }],
                                        
                                        labels: [
                                            "Books",
                                            "Films",
                                            "CDs"
                                        ]

                                    }
                                });
                            </script>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>

    </body>

    <footer>

    </footer>
</html>
