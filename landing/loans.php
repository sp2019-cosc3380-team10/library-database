<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");
?>

<?php
	//A query to get all the loans belonging to the current user
	$user = $_SESSION["UserName"];
	$query_current = "SELECT L.`LoanNo`, M.`Title`, L.`DueDate`, L.`CheckOutDate`
	    FROM `LOANS` AS L, `MEDIA` AS M, `USERS` AS U 
	    WHERE L.`MEDIA_IDMedia` = M.`IDMedia` AND L.`USERS_IDUser` = U.`IDUser` AND U.`UserName` = '$user' AND L.`LoanActive` = 1";
	$query_past = "SELECT L.`LoanNo`, M.`Title`, L.`CheckInDate`, L.`CheckOutDate`
	    FROM `LOANS` AS L, `MEDIA` AS M, `USERS` AS U 
	    WHERE L.`MEDIA_IDMedia` = M.`IDMedia` AND L.`USERS_IDUser` = U.`IDUser` AND U.`UserName` = '$user' AND L.`LoanActive` = 0";
	
	//Connect to database
	$db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL!" . mysqli_connect_error());
	
	//Query database
	$results_current = mysqli_query($db_connection, $query_current) OR die("Unable to query current loan info!" . mysqli_error($db_connection));
	$results_past = mysqli_query($db_connection, $query_past) OR die("Unable to query past loan info!" . mysqli_error($db_connection));
	$num_rows_current = mysqli_num_rows($results_current);
	$num_rows_past = mysqli_num_rows($results_past);
	
	//Store retrieved data
	for($i = 0; $i < $num_rows_current; $i++)
	{
	    $rows_current[$i] = mysqli_fetch_assoc($results_current);
	}
	for($i = 0; $i < $num_rows_past; $i++)
	{
	    $rows_past[$i] = mysqli_fetch_assoc($results_past);
	}
	
	mysqli_close($db_connection); 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Loans</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            // includes navbar.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>
	
	<div class="container">
	    <div class="card card-body">
	        <h2> Current Loans: </h2>
		<hr>
		<div class="row">
			<div class="col-md-2">
			    <h4>Loan #</h4>
			    <?php
			    for($i = 0; $i < $num_rows_current; $i++)
			    {
				echo "<p>" . $rows_current[$i]["LoanNo"] . "</p>"; //print that row of the query result
			    }
			    ?>
			</div>
			<div class="col-md-4">
			    <h4>Title</h4>
			    <?php
			    for($i = 0; $i < $num_rows_current; $i++)
			    {
				echo "<p>" . $rows_current[$i]["Title"] . "</p>"; //print that row of the query result
			    }
			    ?>
			</div>
			<div class="col-md-2">
			    <h4>Checked Out</h4>
			    <?php
			    for($i = 0; $i < $num_rows_current; $i++)
			    {
				echo "<p>" . $rows_current[$i]["CheckOutDate"] . "</p>"; //print that row of the query result
			    }
			    ?>
			</div>
			<div class="col-md-2">
			    <h4>Due By</h4>
			    <?php
			    for($i = 0; $i < $num_rows_current; $i++)
			    {
				echo "<p>" . $rows_current[$i]["DueDate"] . "</p>"; //print that row of the query result
			    }
			    ?>
			</div>
		</div>
	    </div>
	</div>
	
	<div class="container">
	    <div class="accordion" id="accordion">
		<div class="card-header">
			<h6 class="mb-0">
			    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-past">
				Past Loans
			    </button>
			</h6>
		</div>
		<div id="collapse-past" class="collapse" data-parent="#accordion">
			<div class="card card-body">
				<div class="row">
					<div class="col-md-2">
					    <h4>Loan #</h4>
					    <?php
					    for($i = 0; $i < $num_rows_past; $i++)
					    {
						echo "<p>" . $rows_past[$i]["LoanNo"] . "</p>"; //print that row of the query result
					    }
					    ?>
					</div>
					<div class="col-md-4">
					    <h4>Title</h4>
					    <?php
					    for($i = 0; $i < $num_rows_past; $i++)
					    {
						echo "<p>" . $rows_past[$i]["Title"] . "</p>"; //print that row of the query result
					    }
					    ?>
					</div>
					<div class="col-md-2">
					    <h4>Checked Out</h4>
					    <?php
					    for($i = 0; $i < $num_rows_past; $i++)
					    {
						echo "<p>" . $rows_past[$i]["CheckOutDate"] . "</p>"; //print that row of the query result
					    }
					    ?>
					</div>
					<div class="col-md-2">
					    <h4>Checked In</h4>
					    <?php
					    for($i = 0; $i < $num_rows_past; $i++)
					    {
						echo "<p>" . $rows_past[$i]["CheckInDate"] . "</p>"; //print that row of the query result
					    }
					    ?>
					</div>
				</div>
			</div>
		</div>
		
	    </div>
	    
	</div>

    </body>

    <footer>

    </footer>
</html>