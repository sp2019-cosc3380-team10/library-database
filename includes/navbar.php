<!-- Navbar -->
<nav id="navbar-main" class="navbar navbar-dark navbar-expand-sm" style="background-color: #0099ff">
    <a class="navbar-brand" href="/landing/">
        <img src="/images/books.png" width="30px" height="30px"> Team 10 Library
    </a>
    <!-- Search bar -->
    <div class="navbar-nav mx-auto">
        <form action="search.php" method="GET">
            <input style="display: none;" class="form-control" name="SearchBy" value="Title">
            <div class="input-group">
                <input class="form-control" type="text" placeholder="Search catalog..." name="SearchKey">
                <div class="input-group-append">
                    <button class="btn btn-success" type="submit">Search</button>
                </div>
            </div>
        </form>
    </div>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/landing/">
                <i class="fas fa-home"></i> Home
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/landing/account.php">
                <i class="fas fa-user"></i> <?php echo $_SESSION["UserName"]; ?>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/scripts/php/signout.php">
                <i class="fas fa-sign-out-alt"></i> Sign Out
            </a>
        </li>
    </ul>
</nav>