<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/admin_check.php");
    
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");
    $db_connection = @mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die ("Unable to connect to MySQL! " . mysqli_connect_error()); //connection to the database
    $LibraryNo = $_SESSION["LibraryNo"];
    $active_fines_query = "SELECT * FROM `FINES` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo') AND `FineActive`='1';"; //Select all users with active fines
    $active_fines_results = mysqli_query($db_connection, $active_fines_query); //Make connection with database with active fines 
    $active = "SELECT `FineActive` FROM  `FINES`";
    $activeresult = mysqli_query($db_connection, $active);

    $past_fines = "SELECT * FROM `FINES` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo') AND `FineActive`='0';";
    $past_fines_results = mysqli_query($db_connection, $past_fines);


    $row = mysqli_fetch_assoc($activeresult);
    $Booklate = 1.25; //Cost of a book to be late every day
    $Medialate = 1.75; //Cost of media to be late every day 
    $FineType = "SELECT `FineTypeName` FROM `FINE_TYPE`"; 
    $FineTyperesult = mysqli_query($db_connection, $FineType);
    $row2 = mysqli_fetch_assoc($FineTyperesult);


    //queries for Dates
    $Datetime_query = "SELECT `DueDate` FROM `LOANS`, `FINES` WHERE `IDLoan`=`LOANS_IDLoan`";
    $Datetime_results = mysqli_query($db_connection, $Datetime_query);
    $Datetime_row = $Datetime_results->fetch_assoc();

    if (isset($_GET["SearchBy"]) && isset($_GET["SearchKey"]))  //Search
    {
    $select_user = "";
    $SearchBy = $_GET["SearchBy"];
    $SearchKey = $_GET["SearchKey"];
    $search_query = "SELECT `LibraryNo`, `FirstName`, `LastName` FROM `USERS` WHERE $SearchBy LIKE '%$SearchKey%' ORDER BY `$SearchBy`;";
    $search_results = mysqli_query($db_connection, $search_query) or die ("Unable to search!");
    }

    if (isset($_POST["RemoveFine"]))    //If user paid the fine revert FineActive back to 0 
    {
        //$NoFines_alert = '<div class="alert alert-danger"><strong>Success:</strong> User has no fines on their account.</div>';
        $NoFines_user = $_POST["RemoveFine"];
        $deactivate_query = "UPDATE `FINES` SET `FineActive`= 0 WHERE `FineNo` = '$NoFines_user'";        
        mysqli_query($db_connection, $deactivate_query) or die ("Unable to deactivate fine!");
    }
?>



<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Fines Manager</title>

        <?php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>

    </head>

    <body>

        <?php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>


        <div class ="container">
            <div class = "card">

                <div class = "card-header">
                    <h6> Fines Manager </h6>
                </div>

                <div class = "card-body">
                    <form action="" method="POST">                       
                        <?php 

                            if ($active_fines_results->num_rows > 0)
                            {
                                echo '
                                    <table class = "table table-striped table-hover">
                                        <thead class = "thead-dark">
                                            <tr>
                                                <th scope="col"> Fine No. </th>
                                                <th scope="col"> Fine Date</th>
                                                <th scope="col"> Loan No.</th>
                                                <th scope="col"> Media No.</th>
                                                <th scope="col"> Fine Cost</th>
                                                <th scope="col"> Fine Type</th>
                                                <th scope="col"> Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                ';     
                            
                            while ($active_fines_row = $active_fines_results->fetch_assoc())  //grab all the rows
                            {
                                // gets ID of loan
                                $IDLoan = $active_fines_row["LOANS_IDLoan"];
                                // gets ID of fine cost
                                $IDFineCost = $active_fines_row["FINE_COSTS_IDFineCost"];

                                // queries for loan no.
                                $loan_no_query = "SELECT * FROM `LOANS` WHERE `IDLoan`='$IDLoan';";
                                $loan_no_results = mysqli_query($db_connection, $loan_no_query);
                                $loan_no_row = $loan_no_results->fetch_assoc();
                                
                                // gets ID of MEDIA
                                $IDMedia = $loan_no_row["MEDIA_IDMedia"];
                                
                                // queries for media no.
                                $media_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                $media_no_results = mysqli_query($db_connection, $media_no_query);
                                $media_no_row = $media_no_results->fetch_assoc();
                                
                                // queries for fine cost
                                $fine_cost_query = "SELECT * FROM `FINE_COSTS` WHERE `IDFineCost`='$IDFineCost';";
                                $fine_cost_results = mysqli_query($db_connection, $fine_cost_query);
                                $fine_cost_row = $fine_cost_results->fetch_assoc();

                                // gets the ID of fine type
                                $IDFineType = $fine_cost_row["FINE_TYPE_IDFineType"];

                                // queries for fine type
                                $fine_type_query = "SELECT `FineTypeName` FROM `FINE_TYPE` WHERE `IDFineType`='$IDFineType'";
                                $fine_type_results = mysqli_query($db_connection, $fine_type_query);
                                $fine_type_row = $fine_type_results->fetch_assoc();

                                echo '
                                    <tr>
                                        <td>' . $active_fines_row["FineNo"] . '</td>
                                        <td>' . $active_fines_row["FineDate"] . '</td>
                                        <td>' . $loan_no_row["LoanNo"] . '</td>
                                        <td>' . $media_no_row["BarcodeNo"] . '</td>
                                        <td>$' . $fine_cost_row["FineCost"] . '</td>
                                        <td>' . $fine_type_row["FineTypeName"] . '</td>

                                        <td><button class="btn btn-outline-success" type="submit" name="RemoveFine" value="' . $active_fines_row["FineNo"] . '">Clear Fine</button></td>

                                    <tr>
                                ';
                            }
                        
                            echo '<tbody></table>';
                            }
                        ?>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="accordion" id="accordion">
                <div class="card">
                    <div class="card-header">
                        <h6 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-past">
                                Past Fines
                            </button>
                        </h6>
                    </div>
                    <div id="collapse-past" class="collapse" data-parent="#accordion">
                        <div class="card-body">                       
                            <?php
                                if ($past_fines_results->num_rows > 0)
                                {
                                    echo '
                                        <table class = "table table-striped table-hover">
                                            <thead class = "thead-dark">
                                                <tr>
                                                    <th scope="col"> Fine No. </th>
                                                    <th scope="col"> Fine Date</th>
                                                    <th scope="col"> Loan No.</th>
                                                    <th scope="col"> Media No.</th>
                                                    <th scope="col"> Fine Cost</th>
                                                    <th scope="col"> Fine Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';     
                                
                                    while ($past_fines_row = $past_fines_results->fetch_assoc())  //grab all the rows
                                    {
                                        // gets ID of loan
                                        $IDLoan = $past_fines_row["LOANS_IDLoan"];
                                        // gets ID of fine cost
                                        $IDFineCost = $past_fines_row["FINE_COSTS_IDFineCost"];

                                        // queries for loan no.
                                        $loan_no_query = "SELECT * FROM `LOANS` WHERE `IDLoan`='$IDLoan';";
                                        $loan_no_results = mysqli_query($db_connection, $loan_no_query);
                                        $loan_no_row = $loan_no_results->fetch_assoc();
                                        
                                        // gets ID of MEDIA
                                        $IDMedia = $loan_no_row["MEDIA_IDMedia"];
                                        
                                        // queries for media no.
                                        $media_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                        $media_no_results = mysqli_query($db_connection, $media_no_query);
                                        $media_no_row = $media_no_results->fetch_assoc();
                                        
                                        // queries for fine cost
                                        $fine_cost_query = "SELECT * FROM `FINE_COSTS` WHERE `IDFineCost`='$IDFineCost';";
                                        $fine_cost_results = mysqli_query($db_connection, $fine_cost_query);
                                        $fine_cost_row = $fine_cost_results->fetch_assoc();

                                        // gets the ID of fine type
                                        $IDFineType = $fine_cost_row["FINE_TYPE_IDFineType"];

                                        // queries for fine type
                                        $fine_type_query = "SELECT `FineTypeName` FROM `FINE_TYPE` WHERE `IDFineType`='$IDFineType'";
                                        $fine_type_results = mysqli_query($db_connection, $fine_type_query);
                                        $fine_type_row = $fine_type_results->fetch_assoc();

                                        echo '
                                            <tr>
                                                <td>' . $past_fines_row["FineNo"] . '</td>
                                                <td>' . $past_fines_row["FineDate"] . '</td>
                                                <td>' . $loan_no_row["LoanNo"] . '</td>
                                                <td>' . $media_no_row["BarcodeNo"] . '</td>
                                                <td>$' . $fine_cost_row["FineCost"] . '</td>
                                                <td>' . $fine_type_row["FineTypeName"] . '</td>
                                            <tr>
                                        ';
                                    }
                                
                                    echo '<tbody></table>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

    <footer>
    </footer>

</html>