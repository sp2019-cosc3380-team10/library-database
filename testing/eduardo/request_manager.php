<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");
?>

<?php

$db_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL " . mysqli_connect_error());
$sql = "SELECT * FROM REQUESTS WHERE RequestActive = 1";
	
if ($results = mysqli_query($db_connection, $sql))
{
    $num_rows = mysqli_num_rows($results);
    for($i = 0; $i < $num_rows; $i++)
    {
        $rows[$i] = mysqli_fetch_row($result);
    }  
}

//https://stackoverflow.com/questions/19828064/add-days-to-a-date-php

function function_update_request($request_number, $conn){
    $date = date('Y-m-d H:i:s'); 
    $expire_date= date('Y-m-d H:i:s', strtotime($date. ' + 2 days'));
    
    $update_values = "UPDATE `REQUESTS` SET `ReadyDate` ='$date', `ExpireDate`='$expire_date', `RequestActive`= 0 
                        WHERE `RequestNo`='$request_number'";
    mysqli_query($conn, $update_values);
}

function function_cancel_date($request_num, $conn){
    $cancel_date = date('Y-m-d H:i:s');
    $cancel = "UPDATE `REQUESTS` SET `CancelDate` = '$cancel_date', `RequestActive` = 0
                        WHERE `RequestNo` = '$request_num'";


   /*
   $cancel = "UPDATE `REQUESTS` SET `ReadyDate` =NULL, `ExpireDate`=NULL, `CancelDate` = '$cancel_date', `PickUpDate` = NULL, `RequestActive` = 0
                        WHERE `RequestNo` = '$request_num'";
                        */

    mysqli_query($conn, $cancel);
}

//Testing the functions out
    function_update_request('12345678', $db_connection);
    mysqli_close($db_connection); 
?>