<!DOCTYPE html>
<html>
	<head>
		<title>T10LIB - Media Manager</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
	</head>
	<body>
		<?php
			// includes navigation bar and session
            // include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");

            // sets default display cards to none
		    $results_style_modify = "display: none;";
		    $results_style_remove = "display: none;";
		    $info_style = "display: none;";

		    if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"])) {
	        	// remove the style for results card
	        	$results_style_modify = "";
	    	}
	    	if (isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
	        	// remove the style for results card
	        	$results_style_remove = "";
	    	}
        ?>

		<h1>MEDIA MANAGER</h1>
		<hr>

		<!-- *********************** ADD NEW MEDIA LIBRARY ************************** -->

		<div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Add New Media to Library</h6>
                </div>
                <div class="card-body">
					<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
						<div class="form-group">
							<label>Media Type:</label><br>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="customRadioInline1" name="MediaTID" class="custom-control-input" value="1">
								<label class="custom-control-label" for="customRadioInline1">Book</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="customRadioInline2" name="MediaTID" class="custom-control-input" value="2">
								<label class="custom-control-label" for="customRadioInline2">Film</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="customRadioInline3" name="MediaTID" class="custom-control-input" value="3">
								<label class="custom-control-label" for="customRadioInline3">CD</label>
							</div>
						</div>
						<div class="form-group">
							<label>Title of New Media:</label>
							<input type="text" class="form-control" placeholder="Title" name="MediaTitle" required>
						</div>
						<div class="form-group">
							<label>Publish Year:</label>
							<input type="text" class="form-control" placeholder="Year" name="MediaPublishYear" required>
						</div>
						<div class="form-group">
							<label>Quantity to be Received:</label>
							<input type="text" class="form-control" placeholder="Total Quantity" name="TotalQuantity" required>
						</div>
						<button type="Submit" class="btn btn-outline-secondary">Add</button>
					</form>
                </div>
            </div>
        </div>

        <!-- *********************** MODIFY MEDIA ************************** -->

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Modify Media</h6>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-3">
                                <select name="SearchByModify" id="search-by" class="custom-select" required>
                                    <option value="">Search catalog by...</option>
                                    <option value="Title">Title</option>
                                    <option value="BarcodeNo">Barcode Number</option>
                                    <option value="PublishYear">Publish Year</option>
                                    <option value="Creator">Creator</option>
                                    <option value="Genre">Genre</option>
                                    <option value="Publisher">Publisher</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input name="SearchKeyModify" id="search-key" class="form-control" placeholder="Enter a search key..." required>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Search results to be shown in the same card body -->
			        <div class="row" style="<?php echo $results_style_modify; ?>">
			            <form action="" method="GET">
			                <?php
			                    // if SearchBy and SearchKey is set
			                    if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"])) {
			                        // if there are more than one row
			                        if ($search_results->num_rows > 0) {
			                            // prints the table header
			                            echo '
			                                <table class="table table-striped table-hover">
			                                    <thead class="thead-dark">
			                                        <tr>
			                                            <th scope="col">Select</th>
			                                            <th scope="col">Barcode No.</th>
			                                            <th scope="col">Title</th>
			                                            <th scope="col">Publish Year</th>
			                                            <th scope="col">Qty. Available</th>
			                                            <th scope="col">Type</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                            ';

			                            // fetch all the rows
			                            while($search_results_row = $search_results->fetch_assoc()) {
			                                $barcode_no = $search_results_row["BarcodeNo"];

			                                // searches for the media types
			                                $media_type_query = "SELECT MediaTypeName FROM `MEDIA_TYPES`, `MEDIA` WHERE `IDMediaType`=`MEDIA_TYPES_IDMediaType` AND `BarcodeNo`='$barcode_no';";

			                                // queries for media type
			                                $media_type_results = mysqli_query($db_connection, $media_type_query);

			                                $media_type_row = $media_type_results->fetch_assoc();

			                                echo '
			                                <tr>
			                                    <td><button type="submit" name="SelectMedia" value="' . $search_results_row["BarcodeNo"] . '" class="btn btn-outline-secondary">+</button></td>
			                                    <td>' . $search_results_row["BarcodeNo"] . '</td>
			                                    <td>' . $search_results_row["Title"] . '</td>
			                                    <td>' . $search_results_row["PublishYear"] . '</td>
			                                    <td>' . $search_results_row["QuantityAvailable"] . '</td>
			                                    <td>' . $media_type_row["MediaTypeName"] . '</td>
			                                </tr>
			                                ';
			                            }

			                            echo '</tbody></table>';
			                        }
			                        else {
			                            echo '<h4 class="text-center">No search results...</h4>';
			                        }
			                    }
			                ?>
			            </form>
			        </div>
                </div>
            </div>
        </div>

        <!-- *********************** REMOVE MEDIA ************************** -->

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h6>Remove Media</h6>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-3">
                                <select name="SearchByRemove" id="search-by" class="custom-select" required>
                                    <option value="">Search catalog by...</option>
                                    <option value="Title">Title</option>
                                    <option value="BarcodeNo">Barcode Number</option>
                                    <option value="PublishYear">Publish Year</option>
                                    <option value="Creator">Creator</option>
                                    <option value="Genre">Genre</option>
                                    <option value="Publisher">Publisher</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input name="SearchKeyRemove" id="search-key" class="form-control" placeholder="Enter a search key..." required>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Search results to be shown in the same card body -->
			        <div class="row" style="<?php echo $results_style_remove; ?>">
			            <form action="" method="GET">
			                <?php
			                    // if SearchBy and SearchKey is set
			                    if (isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
			                        // if there are more than one row
			                        if ($search_results->num_rows > 0) {
			                            // prints the table header
			                            echo '
			                                <table class="table table-striped table-hover">
			                                    <thead class="thead-dark">
			                                        <tr>
			                                            <th scope="col">Select</th>
			                                            <th scope="col">Barcode No.</th>
			                                            <th scope="col">Title</th>
			                                            <th scope="col">Publish Year</th>
			                                            <th scope="col">Qty. Available</th>
			                                            <th scope="col">Type</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                            ';

			                            // fetch all the rows
			                            while($search_results_row = $search_results->fetch_assoc()) {
			                                $barcode_no = $search_results_row["BarcodeNo"];

			                                // searches for the media types
			                                $media_type_query = "SELECT MediaTypeName FROM `MEDIA_TYPES`, `MEDIA` WHERE `IDMediaType`=`MEDIA_TYPES_IDMediaType` AND `BarcodeNo`='$barcode_no';";

			                                // queries for media type
			                                $media_type_results = mysqli_query($db_connection, $media_type_query);

			                                $media_type_row = $media_type_results->fetch_assoc();

			                                echo '
			                                <tr>
			                                    <td><button type="submit" name="SelectMedia" value="' . $search_results_row["BarcodeNo"] . '" class="btn btn-outline-secondary">+</button></td>
			                                    <td>' . $search_results_row["BarcodeNo"] . '</td>
			                                    <td>' . $search_results_row["Title"] . '</td>
			                                    <td>' . $search_results_row["PublishYear"] . '</td>
			                                    <td>' . $search_results_row["QuantityAvailable"] . '</td>
			                                    <td>' . $media_type_row["MediaTypeName"] . '</td>
			                                </tr>
			                                ';
			                            }

			                            echo '</tbody></table>';
			                        }
			                        else {
			                            echo '<h4 class="text-center">No search results...</h4>';
			                        }
			                    }
			                ?>
			            </form>
			        </div>
                </div>
            </div>
        </div>

	<?php
	    // TODO: MEDIA MANAGER
		//		 Querying code right now from database. Working on adding Media
		//		 Manager functionality.

	    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");

	    $db_connection = @mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) OR die ("Unable to connect to MySQL " . mysqli_connect_error());


	    // TESTING form POST
	    // if ALL required information has been entered
	    if (!empty($_POST)) {
	    	$mediatID = $_POST['MediaTID'];
		    echo "Media Type ID : " . $mediatID . "<br>";
		    $mediaTitle = $_POST['MediaTitle'];
		    echo "Media Title : " . $mediaTitle . "<br>";
		    $barcodeNumber = substr(hash("sha512", $mediaTitle), 0, 8);
		    echo "Barcode Number : ". $barcodeNumber . "<br>";
		    $mediaPublishYear = $_POST['MediaPublishYear'];
		    echo "Media Publish Year : " . $mediaPublishYear . "<br>";
		    // total quantity will be quantity available since 
		    $totalQuantity = $_POST['TotalQuantity'];
		    echo "Total Quantity : " . $totalQuantity . "<br>";
		    $quantityAvailable = $_POST['TotalQuantity'];
		    echo "Quantity Available : " . $quantityAvailable . "<br><br>";
	    }



		/********************** Add new MEDIA ****************************/

		// add new media not existing in library
		// is barcode going to be generated or given as input?
		function insert_media($media_tid, $barcode_num, $media_title, $pub_year, $quant_avail, $total, $conn) {
			$add = "INSERT INTO MEDIA (MEDIA_TYPES_IDMediaType, BarcodeNo, Title, PublishYear, QuantityAvailable, QuantityTotal) VALUES ((SELECT IDMediaType FROM MEDIA_TYPES WHERE IDMediaType=" . $media_tid . "),`" . $barcode_num . "`,`" . $media_title . "`," . $pub_year . "," . $quant_avail . "," . $total . ")";

			if (mysqli_query($conn, $add)) {
		    	echo $media_title . " was added succesfully!";
			} else {
		    	echo "Error: " . $add . "<br>" . mysqli_error($conn) . "<br>";
			}
		}



		/********************* SEARCHING *********************************/
	    // if there are parameters SearchBy and SearchKey
	    if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"]) || isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
	        // remove the style for results card
	        //$results_style = "";

	        // stores GET paramters locally
	        if (isset($_GET["SearchByModify"]) && isset($_GET["SearchKeyModify"])) {
	        	$SearchBy = $_GET["SearchByModify"];
	        	$SearchKey = $_GET["SearchKeyModify"];
	    	}
	    	if (isset($_GET["SearchByRemove"]) && isset($_GET["SearchKeyRemove"])) {
	        	$SearchBy = $_GET["SearchByRemove"];
	        	$SearchKey = $_GET["SearchKeyRemove"];
	    	}

	        if ($SearchBy == "Genre") {
	            $search_query = "SELECT `BarcodeNo`, `Title`, `PublishYear`, `Quantity` FROM `MEDIA`, `MEDIA_has_GENRES`, `GENRES` WHERE
	            ;";
	        }
	        else if ($SearchBy == "Creator") {
	        	// empty...
	        }
	        else if ($SearchBy == "Publisher") {
	        	// empty..
	        }
	        else {
	            // search query to find keys like $SearchKey
	            $search_query = "SELECT `BarcodeNo`, `Title`, `PublishYear`, `QuantityAvailable`, `QuantityTotal` FROM `MEDIA` WHERE `$SearchBy` LIKE '%$SearchKey%';";
	            // queries database
	            $search_results = mysqli_query($db_connection, $search_query) or die ("Unable to query database for search results! " . mysqli_error($db_connection));
	        }
	    }

	    $BarcodeNo = "";

	    // if user selected a media
	    if (isset($_POST["SelectMedia"])) {
	        // changes style so the info card shows
	        $info_style = "";

	        // sets BarcodeNo
	        $BarcodeNo = $_POST["SelectMedia"];

	        /**
	         * SELECT GENERAL MEDIA COLUMNS
	         */

	        // query for media info
	        $media_query = "SELECT * FROM `MEDIA` WHERE `BarcodeNo`='$BarcodeNo';";

	        // queries for media
	        $media_results = mysqli_query($db_connection, $media_query);

	        // grabs a row
	        $media_row = $media_results->fetch_assoc();

	        // grabs IDMedia
	        $IDMedia = $media_row["IDMedia"];

	        /**
	         * SELECT MEDIA TYPE
	         */
	        // selects media type name
	        $media_type_query = "SELECT `MediaTypeName` FROM `MEDIA_TYPES`, `MEDIA` WHERE `MEDIA_TYPES_IDMediaType`=`IDMediaType`;";

	        // queries for media type
	        $media_type_results = mysqli_query($db_connection, $media_type_query);
	        
	        // grabs media type row
	        $media_type_row = $media_type_results->fetch_assoc();

	        /**
	         * SELECT CREATORS
	         */

	        // query to select creators
	        $creators_query = "SELECT `FirstName`, `LastName`, `CreatorType` FROM `CREATORS`, `MEDIA_has_CREATORS` WHERE `MEDIA_IDMedia`='$IDMedia' AND `CREATORS_IDCreator`=`IDCreator`;";

	        // queries for creators
	        $creators_results = mysqli_query($db_connection, $creators_query) or die("Unable to query genres! " . mysqli_error($db_connection));

	        /**
	         * SELECT GENRES
	         */
	        // query to select genres
	        $genres_query = "SELECT `GenreName` FROM `GENRES`, `MEDIA_has_GENRES` WHERE `MEDIA_IDMedia`='$IDMedia' AND `GENRES_IDGenre`=`IDGenre`;";

	        // queries for genres
	        $genres_results = mysqli_query($db_connection, $genres_query) or die("Unable to query genres! " . mysqli_error($db_connection));

	        /**
	         * SELECT PUBLISHERS
	         */
	        // query to select publisher names
	        $publishers_query = "SELECT `PublisherName` FROM `PUBLISHERS`, `MEDIA_has_PUBLISHERS` WHERE `MEDIA_IDMedia`='$IDMedia' AND `PUBLISHERS_IDPublisher`=`IDPublisher`;";

	        // queries for publishers
	        $publishers_results = mysqli_query($db_connection, $publishers_query) or die("Unable to query genres! " . mysqli_error($db_connection));
	    }


		/************************ Modify MEDIA *************************/
		// Modify attributes for media - we want to modify Title, PublishYear, Quantity, and IDMediaType

		// add media existent in library
		function add_media($barcode_num, $media_title, $pub_year, $inc, $conn) {
			$add = "UPDATE MEDIA SET QuantityAvailable=QuantityAvailable+" . $inc . ", QuantityTotal=QuantityTotal+" . $inc . " WHERE (BarcodeNo='" . $barcode_num . "' AND Title='" . $media_title . "' AND PublishYear=" . $pub_year . ")";

			if (mysqli_query($conn, $add)) {
			    echo $inc . " books of " . $media_title . " were added.";
			} else {
			    echo "Error: " . $add . "<br>" . mysqli_error($conn) . "<br>";
			}
		}

		// Modify Title of existing media
		function mod_title($barcode_num, $new_title, $pub_year, $conn) {
			$mod = "UPDATE MEDIA SET Title=" . $new_title . " WHERE (BarcodeNo='" . $barcode_num . "' AND PublishYear=" . $pub_year . ")";

			if (mysqli_query($conn, $mod)) {
			    echo "New title of book is " . $new_title . ".<br>";
			} else {
			    echo "Error: " . $mod . "<br>" . mysqli_error($conn) . "<br>";
			}
		}

		// Modify PublishYear of existing media
		function mod_pub_year($barcode_num, $media_title, $new_pub_year, $conn) {
			$mod = "UPDATE MEDIA SET PublishYear=" . $new_pub_year . " WHERE (BarcodeNo='" . $barcode_num . "' AND Title='" . $media_title . "')";

			if (mysqli_query($conn, $mod)) {
			    echo "New publish year of " . $media_title . " is " . $new_pub_year . ".<br>";
			} else {
			    echo "Error: " . $mod . "<br>" . mysqli_error($conn) . "<br>";
			}
		}

		// Removes some media existent in library (does not delete)
		function remove_media($barcode_num, $media_title, $pub_year, $dec, $conn) {
			// ...check if $dec will lower quantity below zero
			// ...code here

			$remove = "UPDATE MEDIA SET QuantityAvailable=QuantityAvailable-" . $dec . ", QuantityTotal=QuantityTotal-" . $dec . " WHERE (BarcodeNo='" . $barcode_num . "' AND Title='" . $media_title . "' AND PublishYear=" . $pub_year . ")";

			if (mysqli_query($conn, $remove)) {
			    echo $dec . " books of " . $media_title . " were removed.<br>";
			} else {
			    echo "Error: " . $remove . "<br>" . mysqli_error($conn) . "<br>";
			}
		}

		// Modify MEDIA_TYPES_IDMediaType (book, cd, movie) existent in library
		function mod_id_type($new_media_tid, $barcode_num, $media_title, $pub_year, $conn) {
			$mod = "UPDATE MEDIA SET MEDIA_TYPES_IDMediaType=" . $new_media_tid . " WHERE (BarcodeNo='" . $barcode_num . "' AND Title='" . $media_title . "' AND PublishYear=" . $pub_year . ")";

			if (mysqli_query($conn, $mod)) {
			    echo "Media type for " . $media_title . " has been modified.<br>";
			} else {
			    echo "Error: " . $mod . "<br>" . mysqli_error($conn) . "<br>";
			}
		}

		// Deletes media from library by decreasing quantity to zero
		// Otherwise unavailable
		function delete_media($barcode_num, $media_title, $pub_year, $conn) {
			$mod = "UPDATE MEDIA SET QuantityTotal=0, QuantityAvailable=0 WHERE (BarcodeNo='" . $barcode_num . "' AND Title='" . $media_title . "' AND PublishYear=" . $pub_year . ")";

			if (mysqli_query($conn, $mod)) {
			    echo $media_title . " has been removed from library until further notice.<br>";
			} else {
			    echo "Error: " . $mod . "<br>" . mysqli_error($conn) . "<br>";
			}
		}

		/********************** TESTING functions **********************/

		// test insert new book -- passes
		/*insert_media($mediatID, $barcodeNumber, $mediaTitle, $mediaPublishYear, $quantityAvailable, $totalQuantity, $db_connection);*/

		// test increasing quantity -- passes
		/*add_media($barcodeNumber, $mediaTitle, $mediaPublishYear, 3, $db_connection);*/
		// assume decreasing quantity passes as well


		mysqli_close($db_connection);
	?>

	</body>
</html>