<?php
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/session_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/admin_check.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/db_credentials.php");
$db_connection = @mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die ("Unable to connect to MySQL! " . mysqli_connect_error()); //connection to the database
$LibraryNo = $_SESSION["LibraryNo"];

$active_fines_query = "SELECT * FROM `FINES` WHERE `USERS_IDUser`=(SELECT `IDUser` FROM `USERS` WHERE `LibraryNo`='$LibraryNo') AND `FineActive`='1';"; //Select all users with active fines
$active_fines_results = mysqli_query($db_connection, $active_fines_query); //Make connection with database with active fines 
$active = "SELECT `FineActive` FROM  `FINES`";
$activeresult = mysqli_query($db_connection, $active);
$row = mysqli_fetch_assoc($activeresult);
$Booklate = 1.25; //Cost of a book to be late every day
$Medialate = 1.75; //Cost of media to be late every day 
$FineType = "SELECT `FineTypeName` FROM `FINE_TYPE`"; 
$FineTyperesult = mysqli_query($db_connection, $FineType);
$row2 = mysqli_fetch_assoc($FineTyperesult);
  //queries for Dates
$Datetime_query = "SELECT `DueDate` FROM `LOANS`, `FINES` WHERE `FineActive`= 1 AND `IDLoan`=`LOANS_IDLoan`";
$Datetime_results = mysqli_query($db_connection, $Datetime_query);
$Datetime_row = $Datetime_results->fetch_assoc();
while ($active_fines_row = $active_fines_results->fetch_assoc())  //grab all the rows
                            {
                                // gets ID of loan
                                $IDLoan = $active_fines_row["LOANS_IDLoan"];
                                // gets ID of fine cost
                                $IDFineCost = $active_fines_row["FINE_COSTS_IDFineCost"];

                                // queries for loan no.
                                $loan_no_query = "SELECT * FROM `LOANS` WHERE `IDLoan`='$IDLoan';";
                                $loan_no_results = mysqli_query($db_connection, $loan_no_query);
                                $loan_no_row = $loan_no_results->fetch_assoc();
                                
                                // gets ID of MEDIA
                                $IDMedia = $loan_no_row["MEDIA_IDMedia"];
                                
                                // queries for media no.
                                $media_no_query = "SELECT `BarcodeNo` FROM `MEDIA` WHERE `IDMedia`='$IDMedia';";
                                $media_no_results = mysqli_query($db_connection, $media_no_query);
                                $media_no_row = $media_no_results->fetch_assoc();
                                
                                // queries for fine cost
                                $fine_cost_query = "SELECT * FROM `FINE_COSTS` WHERE `IDFineCost`='$IDFineCost';";
                                $fine_cost_results = mysqli_query($db_connection, $fine_cost_query);
                                $fine_cost_row = $fine_cost_results->fetch_assoc();

                                // gets the ID of fine type
                                $IDFineType = $fine_cost_row["FINE_TYPE_IDFineType"];

                                // queries for fine type
                                $fine_type_query = "SELECT `FineTypeName` FROM `FINE_TYPE` WHERE `IDFineType`='$IDFineType'";
                                $fine_type_results = mysqli_query($db_connection, $fine_type_query);
                                $fine_type_row = $fine_type_results->fetch_assoc();

            echo '
            <tr>
            <td>' . $past_fines_row["FineNo"] . '</td>
            <td>' . $active_fines_row["FineDate"] . '</td>
            <td>' . $loan_no_row["LoanNo"] . '</td>
            <td>' . $media_no_row["BarcodeNo"] . '</td>
            <td>$' . $fine_cost_row["FineCost"] . '</td>
            <td>' . $fine_type_row["FineTypeName"] . '</td>
            <td>' . $Datetime_row["DueDate"] . '</td>
<tr>
';
    
        }
    

        if (isset($_GET["SearchBy"]) && isset($_GET["SearchKey"]))  //Search
        {
            $select_user = "";
            $SearchBy = $_GET["SearchBy"];
            $SearchKey = $_GET["SearchKey"];
            
            // searches search by similar to search key
            $search_query = "SELECT `LibraryNo`, `FirstName`, `LastName` FROM `USERS` WHERE $SearchBy LIKE '%$SearchKey%' ORDER BY `$SearchBy`;";
        
            // gets search results
            $search_results = mysqli_query($db_connection, $search_query) or die ("Unable to search!");
        }

        if (isset($_POST["RemoveFine"]))    //If user paid the fine revert FineActive back to 0 
        {
            // sets deactivate alert
            $NoFines_alert = '<div class="alert alert-danger"><strong>Success:</strong> User has no fines on their account.</div>';
    
            // grabs GET variable
            $NoFines_user = $_POST["RemoveFine"];
    
            // query to set no fine to user
            $deactivate_query = "UPDATE `FINES` SET `FineActive`= 0 WHERE `LibraryNo`='$NoFines_user';";
    
            // updates user in database
            mysqli_query($db_connection, $deactivate_query) or die ("Unable to deactivate user! " . mysqli_error($db_connection));
        }

mysqli_close($db_connection);
?>


<!DOCTYPE html>
<html>
    <head>
        <title>T10LIB - Fines Manager</title>
        <?php
            // includes header.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/header.php");
        ?>
    </head>

    <body>
        <?php
            // includes navbar.php
            include($_SERVER["DOCUMENT_ROOT"] . "/includes/navbar.php");
        ?>
    </body>

    <footer>

    </footer>
</html>
