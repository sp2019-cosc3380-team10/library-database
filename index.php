<?php
    session_start();
    // if signin session exists, redirect to landing page
    if (isset($_SESSION["signin_session"]))
    {
        header("Location: /landing/");
    }
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/signin.php");
    include($_SERVER["DOCUMENT_ROOT"] . "/scripts/php/register.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Alvin Huynh">

        <link rel="icon" href="./images/books.ico">
        <title>Team 10 Library</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="./bootstrap/4.3.1/css/bootstrap.min.css">
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <!-- Popper.js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <!-- Bootstrap.js -->
        <script src="./bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <!-- Font Awesome icons -->
        <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js"
            integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ"
            crossorigin="anonymous"></script>

        <!-- Custom CSS -->
        <link rel="stylesheet" href="./style.css">

        <!-- Custom JavaScript --> 
        <script src="./scripts/validation.js"></script>

        <script type="text/javascript">
            var __DEBUG = true;     // if true, print out debugging messages (errors and warnings)
            var __VERBOSE = true;   // if true, print out EVERYTHING
            var open_signin = <?php echo $_COOKIE["open_signin"]; ?>;   // if true, open signin modal
            var open_register = <?php echo $_COOKIE["open_register"]; ?>;

            $(document).ready(function() {
                
                // if open_signin, click on sign in modal link
                if (open_signin)
                {
                    document.getElementById("signin-link").click();
                }
            });
        </script>
    </head>

    <body>
        <!-- Navbar -->
        <nav id="navbar-main" class="navbar navbar-dark navbar-expand-sm" style="background-color: #0099ff">
            <a class="navbar-brand" href="#">
                <img src="./images/books.png" width="30px" height="30px"> Team 10 Library
            </a>
            <!-- Search bar -->
            <div class="navbar-nav mx-auto">
                <div class="input-group">
                    <input class="form-control" type="search" placeholder="Search catalog..." aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">Search</button>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav ml-auto">
                <!-- Sign in links -->
                <li class="nav-item">
                    <a class="nav-link" id="signin-link" href="#" data-toggle="modal" data-target="#signin-modal">
                        <i class="fas fa-user"></i> Sign In
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="register-link" href="#" data-toggle="modal" data-target="#register-modal">
                        <i class="fas fa-edit"></i> Register
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Sign in modal -->
        <div class="modal fade" id="signin-modal" tabindex="-1" role="dialog" aria-labelledby="signin-modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Sign In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="signin-username">Username or Library No.</label>
                                <input type="text" class="form-control" id="signin-username" name="signin-username" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <label for="signin-password">Password</label>
                                <input type="password" class="form-control" id="signin-password" name="signin-password" placeholder="Password" required>
                            </div>
                            <p class="text-danger">
                                <?php echo $login_error; ?>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button name="signin" type="submit" class="btn btn-primary">Sign In</button>
                            <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Register modal -->
        <div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="register-modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Register</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="register-username">Username</label>
                                <input type="text" class="form-control" id="register-username" name="register-username" placeholder="Username" maxlength="20" required>
                            </div>
                            <div class="form-group">
                                <label for="register-email">Email</label>
                                <input type="email" class="form-control" id="register-email" name="register-email" placeholder="Email" maxlength="320" required>
                            </div>
                            <div class="form-group" required>
                                <label for="register-password">Password</label>
                                <input type="password" class="form-control" id="register-password" name="register-password" placeholder="Password" minlength="10" maxlength="128" required>
                            </div>
                            <div class="form-group" required>
                                <label for="register-confirm">Confirm Password</label>
                                <input type="password" class="form-control" id="register-confirm" name="register-confirm" placeholder="Password" minlength="10" maxlength="128" required>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="register-firstname">First Name</label>
                                    <input type="text" class="form-control" id="register-firstname" name="register-firstname" placeholder="First Name" maxlength="50" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="register-lastname">Last Name</label>
                                    <input type="text" class="form-control" id="register-lastname" name="register-lastname" placeholder="Last Name" maxlength="50" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="register-birthdate">Birthday</label>
                                <input type="date" class="form-control" id="register-birthdate" name="register-birthdate" required>
                            </div>
                            <div class="form-group">
                                <label for="register-address">Street Address</label>
                                <input type="address" class="form-control" id="register-address" name="register-address" placeholder="Street Address" maxlength="100" required>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="register-city">City</label>
                                    <input type="city" class="form-control" id="register-city" name="register-city" placeholder="City" maxlength="50" required>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="register-zip">Zip</label>
                                    <input type="text" class="form-control" id="register-zip" name="register-zip" placeholder="Zip" maxlength="5" required>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="register-state">State</label>
                                    <select class="form-control" id="register-state" name="register-state" required>
                                        <option selected hidden disabled>Select a state...</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button name="register" type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <br>

    </body>

    <footer>

    </footer>

</html>